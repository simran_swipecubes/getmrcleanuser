package com.example.cleaner.util

import android.animation.AnimatorInflater
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import com.example.cleaner.R

class MyDialogBox(context: Context) : Dialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //  window.setBackgroundDrawableResource(R.color.trans)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.progress_dialog)
        //  val anim=AnimationUtils.loadAnimation(context,R.anim.flip)
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val anim = AnimatorInflater.loadAnimator(context, R.animator.obj_flip)
        //  anim.setTarget(progress_dialog_logo)
        anim.start()
        setCancelable(false)
    }
}