package com.example.cleaner.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.*;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cleaner.R;
import com.example.cleaner.adapter.AddressSpinnerAdapter;
import com.example.cleaner.adapter.DiscountAdapter;
import com.example.cleaner.apiservice.CustomCallBacks;
import com.example.cleaner.apiservice.RestServiceConnector;
import com.example.cleaner.model.CommonModel;
import com.example.cleaner.model.GetAddressModel;
import com.example.cleaner.util.SharedPref;

import retrofit.RetrofitError;
import retrofit.client.Response;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class BookAppointmentActivity extends AppCompatActivity implements View.OnClickListener {
    private Context mContext = this;
    private TextView actionbar_title, tv_totalServicePrice, tv_booking_continue, tv_tax, tv_add_address, tv_address_text;
    private ImageView action_bar_back;
    private String[] pieces = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
    private Spinner pieces_spinner, select_service_spinner;
    Intent intent;
    private String pieces_count, selected_address_id = "", service_price,
            methods_id, unit_id, service_id, sub_servicesArray, time = "";
    SharedPref pref;
    int total, count, price_aft_discount, tax, applied_tax, discount_total, add_on_service;
    RadioGroup radio_group;
    RadioButton rb_cod, rb_online;
    Date currentTime;
    TimePicker simpleTimePicker;
    EditText dateEdittext;
    EditText tv_time;
    RecyclerView rv_discount;
    ImageView iv_down;
    boolean is_tax_applied = false;
    RelativeLayout rl_tax, rl_address_spinner;
    AddressSpinnerAdapter adapter;
    AlertDialog dialog2;
    String final_total;
    int minlimit, maxlimit;
    ProgressDialog progressDialog;
    List<String> pieces_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_appointment_copy);

        initUi();
    }

    private void initUi() {
        intent = getIntent();
        pieces_list = new ArrayList<>();
        pref = new SharedPref(this);
        service_price = intent.getStringExtra("service_price");
        if (intent.getStringExtra("sub_servicesArray") != null) {
            Log.e("Array", intent.getStringExtra("sub_servicesArray"));
        }
        actionbar_title = findViewById(R.id.actionbar_title);
        rl_tax = findViewById(R.id.rl_tax);
        tv_add_address = findViewById(R.id.tv_add_address);
        rl_address_spinner = findViewById(R.id.rl_address_spinner);
        tv_address_text = findViewById(R.id.tv_address_text);
        tv_tax = findViewById(R.id.tv_tax);
        tv_time = findViewById(R.id.tv_time);
        rb_cod = findViewById(R.id.rb_cod);
        rb_online = findViewById(R.id.rb_online);
        radio_group = findViewById(R.id.radio_group);
        dateEdittext = findViewById(R.id.dateEdittext);
        simpleTimePicker = findViewById(R.id.simpleTimePicker);

        tv_booking_continue = findViewById(R.id.tv_booking_continue);
        tv_totalServicePrice = findViewById(R.id.tv_totalServicePrice);
        action_bar_back = findViewById(R.id.action_bar_back);
        pieces_spinner = findViewById(R.id.pieces_spinner);
        rv_discount = findViewById(R.id.rv_discount);
        iv_down = findViewById(R.id.iv_down);
        select_service_spinner = findViewById(R.id.select_service_spinner);
        actionbar_title.setText(getResources().getString(R.string.book_apponitment));
        methods_id = intent.getStringExtra("methods_id");
        unit_id = intent.getStringExtra("unit_id");
        service_id = intent.getStringExtra("service_id");
        tv_totalServicePrice.setText("$ " + intent.getStringExtra("total"));

        action_bar_back.setOnClickListener(this);
        tv_time.setOnClickListener(this);
        tv_booking_continue.setOnClickListener(this);
        dateEdittext.setOnClickListener(this);
        iv_down.setOnClickListener(this);
        tv_add_address.setOnClickListener(this);

        simpleTimePicker.setIs24HourView(false);
        simpleTimePicker.setCurrentHour(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
        currentTime = Calendar.getInstance().getTime();

        final int h = currentTime.getHours();
        final int m = currentTime.getMinutes();

        Date date = new Date();
        Log.e("date", date + "");

        Log.e("service_price", intent.getStringExtra("service_price"));

        simpleTimePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker timePicker, int selectedHour, int selectedMinute) {
                if (dateEdittext.getText().toString().equals("")) {
                    Toast.makeText(mContext, "Please select date first", Toast.LENGTH_SHORT).show();
                    time = h + ":" + m;
                    simpleTimePicker.setCurrentHour(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
                } else {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    Date strDate = null;
                    try {
                        strDate = sdf.parse(dateEdittext.getText().toString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                    Date dateobj = new Date();
                    System.out.println(df.format(dateobj));
                    int date1 = dateobj.getDate();
                    int date2 = strDate.getDate();

                    if (date1 == date2) {
                        time = selectedHour + ":" + selectedMinute;
                        if (checktimings(time, "hh:mm")) {
                            time = selectedHour + ":" + selectedMinute;
                            Log.e("Time", time);
                        } else {
                            time = "";
                            Toast.makeText(mContext, "You can not select past time", Toast.LENGTH_SHORT).show();
                            simpleTimePicker.setCurrentHour(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
                            Log.e("Time", time);
                        }
                    } else {
                        time = selectedHour + ":" + selectedMinute;
                        Log.e("Time", time);
                    }
                }
            }
        });

        pieces_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                pieces_count = pieces[position];
                Log.e("pieces_count", pieces_count);
                count = Integer.valueOf(pieces_count);
                total = Integer.valueOf(intent.getStringExtra("service_price")) * count;
                add_on_service = Integer.valueOf(intent.getStringExtra("total")) - Integer.valueOf(intent.getStringExtra("service_price"));
                total = total + tax + add_on_service;
                tv_totalServicePrice.setText("$ " + total);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        rb_cod.setChecked(true);

        addressAPI();


        //select_service_spinner.setPopupBackgroundResource(R.drawable.stroke_white);
    }

    private boolean checktimings(String time, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
        Date EndTime = null;
        try {
            EndTime = dateFormat.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Date CurrentTime = null;
        try {
            CurrentTime = dateFormat.parse(dateFormat.format(new Date()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (CurrentTime.before(EndTime)) {
            return true;
        } else {
            return false;
        }
    }

    // ********** address APi **********
    private void addressAPI() {
        Map<String, String> data = new HashMap<String, String>();
        data.put("uid", pref.getUSER_ID());
        data.put("sid", service_id);

        RestServiceConnector.getService().getAddressList(data, GetAddressListcallBacks());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.action_bar_back:
                finish();
                break;
            case R.id.tv_time:
                openTimePicker();
                break;
            case R.id.tv_booking_continue:
                validations();
                break;
            case R.id.dateEdittext:
                onDobClick();
                break;
            case R.id.iv_down:
                select_service_spinner.performClick();
                break;
            case R.id.tv_add_address:
                startActivity(new Intent(mContext, AddAddressActivity.class).putExtra("where", "booking"));
                finish();
        }
    }

    // ************ validation *********
    private void validations() {
        if (dateEdittext.getText().toString().isEmpty()) {
            Toast.makeText(mContext, "Please select date", Toast.LENGTH_SHORT).show();
        } else if (tv_time.getText().toString().isEmpty()) {
            time = tv_time.getText().toString();
            Toast.makeText(mContext, "Please select time", Toast.LENGTH_SHORT).show();
        } else if (selected_address_id.equals("")) {
            Toast.makeText(mContext, "Please Select Address", Toast.LENGTH_SHORT).show();
        }/* else if (rb_online.isChecked()) {
            Toast.makeText(mContext, "You can't choose online Option at this moment", Toast.LENGTH_LONG).show();
              }*/ else {
            if (rb_online.isChecked()) {
                String data = null;
                String t = tv_totalServicePrice.getText().toString();
                String final_total = t.replace("$ ", "");
                Log.e("newString", final_total);
                if (final_total.contains(" Incl tax")) {
                    final_total = final_total.replace(" Incl tax", "");/* */
                }
                Log.e("final_total", final_total);
                sub_servicesArray = intent.getStringExtra("sub_servicesArray");
                if (sub_servicesArray == null) {
                    sub_servicesArray = "";
                }
                Log.e("final_total", "sub_servicesArray" + sub_servicesArray);

                try {
                    data = URLEncoder.encode("addressid", "UTF-8") + "=" + URLEncoder.encode(selected_address_id, "UTF-8");
                    data += "&" + URLEncoder.encode("service_id", "UTF-8") + "=" + URLEncoder.encode(service_id, "UTF-8");
                    data += "&" + URLEncoder.encode("method_id", "UTF-8") + "=" + URLEncoder.encode(methods_id, "UTF-8");
                    data += "&" + URLEncoder.encode("uid", "UTF-8") + "=" + URLEncoder.encode(pref.getUSER_ID(), "UTF-8");
                    data += "&" + URLEncoder.encode("method_unit_id", "UTF-8") + "=" + URLEncoder.encode(unit_id, "UTF-8");
                    data += "&" + URLEncoder.encode("piece", "UTF-8") + "=" + URLEncoder.encode(pieces_count, "UTF-8");
                    data += "&" + URLEncoder.encode("total", "UTF-8") + "=" + URLEncoder.encode(final_total, "UTF-8");
                    data += "&" + URLEncoder.encode("bookdate", "UTF-8") + "=" + URLEncoder.encode(dateEdittext.getText().toString(), "UTF-8");
                    data += "&" + URLEncoder.encode("starttime", "UTF-8") + "=" + URLEncoder.encode(time, "UTF-8");
                    data += "&" + URLEncoder.encode("addonservice", "UTF-8") + "=" + URLEncoder.encode(sub_servicesArray, "UTF-8");
                    data += "&" + URLEncoder.encode("mainprice", "UTF-8") + "=" + URLEncoder.encode(service_price, "UTF-8");
                    data += "&" + URLEncoder.encode("discount", "UTF-8") + "=" + URLEncoder.encode(price_aft_discount + "", "UTF-8");
                    data += "&" + URLEncoder.encode("tax", "UTF-8") + "=" + URLEncoder.encode(tax + "", "UTF-8");

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                WebView webview = new WebView(this);
                setContentView(webview);
                final String url = "http://swipecubes.com/carpet/test.php";
                webview.getSettings().setJavaScriptEnabled(true);

                progressDialog = new ProgressDialog(BookAppointmentActivity.this);
                progressDialog.setMessage("Loading Please wait...");
                progressDialog.setCancelable(false);
                progressDialog.show();
                webview.postUrl(url, data.getBytes());
             /*   webview.setWebViewClient(new WebViewClient() {

                    public void onPageFinished(WebView view, String url) {
                        try {
                            progressDialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                    }
                });*/

                webview.setWebViewClient(new WebViewClient() {
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String urlinterna) {
                        if (urlinterna.contains("http://swipecubes.com/carpet/success.php")) {
                            // finish();
                            orderPlacedSuccessfullyDialog();
                            // This is my website, so do not override; let my WebView load the page
                            return true;
                        } else {
                            Toast.makeText(mContext, "Web view", Toast.LENGTH_SHORT).show();
                        }
                        return true;
                    }

                    @Override
                    public void onPageFinished(WebView view, String url) {
                        try {
                            progressDialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            } else {
                String t = tv_totalServicePrice.getText().toString();
                String final_total = t.replace("$ ", "");
                Log.e("newString", final_total);
                if (final_total.contains(" Incl tax")) {
                    final_total = final_total.replace(" Incl tax", "");/* */
                }
                Log.e("final_total", final_total);
                sub_servicesArray = intent.getStringExtra("sub_servicesArray");
                RestServiceConnector.getService().booking(selected_address_id, service_id,
                        methods_id, pref.getUSER_ID(), unit_id, pieces_count, final_total,
                        dateEdittext.getText().toString(), time, sub_servicesArray, service_price, price_aft_discount + "", tax + "",
                        bookingCalling());
            }
        }
    }

    public void onDobClick() {
        final Calendar calendar = Calendar.getInstance();
        DatePickerDialog dialog = new DatePickerDialog(this, R.style.DialogTheme,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker arg0, int year, int month, int day_of_month) {
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, (month));
                        calendar.set(Calendar.DAY_OF_MONTH, day_of_month);
                        String myFormat = "yyyy-MM-dd";
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
                        dateEdittext.setText(sdf.format(calendar.getTime()));
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        dialog.getDatePicker().setMinDate(calendar.getTimeInMillis());// TODO: used to hide previous date,month and year
        calendar.add(Calendar.YEAR, 0);
        calendar.add(Calendar.DAY_OF_MONTH, 60);
        dialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());// TODO: used to hide future date,month and year
        dialog.show();
    }

    //  ********************************** Open Time Picker **************************
    void openTimePicker() {
        TimePickerDialog mTimePicker;
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        mTimePicker = new TimePickerDialog(BookAppointmentActivity.this, android.app.AlertDialog.THEME_HOLO_LIGHT,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        time = selectedHour + ":" + selectedMinute;
                        if (dateEdittext.getText().toString().equals("")) {
                            Toast.makeText(mContext, "Please select date first", Toast.LENGTH_SHORT).show();
                            //time = h + ":" + m;
                            // simpleTimePicker.setCurrentHour(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
                        } else {
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                            Date strDate = null;
                            try {
                                strDate = sdf.parse(dateEdittext.getText().toString());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                            Date dateobj = new Date();
                            System.out.println(df.format(dateobj));
                            int date1 = dateobj.getDate();
                            int date2 = strDate.getDate();
                            Log.e("date1", date1 + "");
                            Log.e("date2", date2 + "");
                            if (date1 == date2) {
                                time = selectedHour + ":" + selectedMinute;
                                if (checktimings(time, "hh:mm")) {
                                    time = selectedHour + ":" + selectedMinute;
                                    Log.e("Time", time);
                                } else {
                                    time = "";
                                    Toast.makeText(mContext, "You can not select past time", Toast.LENGTH_SHORT).show();
                                    simpleTimePicker.setCurrentHour(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
                                    Log.e("Time", time);
                                }
                                tv_time.setText(time);
                            } else {
                                time = selectedHour + ":" + selectedMinute;
                                Log.e("Time", time);
                                tv_time.setText(time);
                            }
                        }
                    }
                }, hour, minute, true);//No 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    // **************** get Address List APi is here **************
    private CustomCallBacks<GetAddressModel> GetAddressListcallBacks() {
        return new CustomCallBacks<GetAddressModel>(mContext, true) {
            @Override
            public void onSucess(GetAddressModel arg0, Response arg1) {
                if (arg0.getStatus() == 200) {

                    minlimit = Integer.parseInt(arg0.getPiece().get(0).getMinlimit());
                    maxlimit = Integer.parseInt(arg0.getPiece().get(0).getMaxlimit());

                    for (int i = minlimit; i <= maxlimit; i++) {
                        pieces_list.add(String.valueOf(i));
                        Log.e("pieces_list", pieces_list + "");
                    }
                    Log.e("pieces_list", pieces_list + "");
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(BookAppointmentActivity.this, R.layout.support_simple_spinner_dropdown_item, pieces_list);
                    // pieces_spinner.setPopupBackgroundResource(R.drawable.stroke_white);
                    pieces_spinner.setAdapter(dataAdapter);

                    if (arg0.getTax().getStatus().equals("Y")) {
                        is_tax_applied = true;
                        tax = Integer.valueOf(arg0.getTax().getValue());
                        if (arg0.getTax().getType().equals("F")) {
                            total = total + tax;
                        } else {
                            applied_tax = (total * tax) / 100;
                            total = total + applied_tax;
                        }
                        rl_tax.setVisibility(View.VISIBLE);
                        tv_tax.setText("$  " + tax);
                        tv_totalServicePrice.setText("$ " + total);
                    }
                    if (arg0.getData().size() != 0) {
                        rl_address_spinner.setVisibility(View.VISIBLE);
                        tv_add_address.setVisibility(View.GONE);
                        adapter = new AddressSpinnerAdapter(getApplicationContext(), arg0.getData(), new AddressSpinnerAdapter.onClickListener() {
                            @Override
                            public void onClicK(String id, int i) {
                                selected_address_id = id;
                                if (!selected_address_id.equals("")) {
                                    if (select_service_spinner != null) {
                                        try {
                                            select_service_spinner.setSelection(i);
                                            Method method = Spinner.class.getDeclaredMethod("onDetachedFromWindow");
                                            method.setAccessible(true);
                                            method.invoke(select_service_spinner);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }

                           /* // ****************** no address selected than it gets the first  address *************
                            @Override
                            public void addressOnZeroIndex(String id) {
                                selected_address_id = id;
                                if (!selected_address_id.equals("")) {
                                    if (select_service_spinner != null) {
                                        try {
                                            select_service_spinner.setSelection(0);
                                         *//*   Method method = Spinner.class.getDeclaredMethod("onDetachedFromWindow");
                                            method.setAccessible(true);
                                            method.invoke(select_service_spinner);*//*
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }*/
                        });

                        select_service_spinner.setPopupBackgroundResource(R.drawable.stroke_white);
                        select_service_spinner.setAdapter(adapter);
                    } else {
                        rl_address_spinner.setVisibility(View.GONE);
                        tv_add_address.setVisibility(View.VISIBLE);
                        tv_address_text.setText("Add address First to Continue Booking");
                    }

                    rv_discount.setLayoutManager(new GridLayoutManager(mContext, 2));
                    rv_discount.setAdapter(new DiscountAdapter(mContext, arg0.getDiscount(), new DiscountAdapter.onClickListener() {
                        @Override
                        public void onClick(String rates, String dType, String status) {
                            Float percentage_amount = Float.valueOf(rates);
                            //int amount = Integer.valueOf(percentage_amount);

                            if (status.equals("true")) {
                                if (!rates.equals(0)) {
                                    if (dType.equals("P")) {
                                        price_aft_discount = (int) ((total * percentage_amount) / 100);
                                        discount_total = total - price_aft_discount;

                                    } else {
                                        price_aft_discount = (int) (total - percentage_amount);
                                        discount_total = total - price_aft_discount;
                                    }
                                    if (is_tax_applied == true) {
                                        tv_totalServicePrice.setText("$ " + discount_total);
                                    } else {
                                        tv_totalServicePrice.setText("$ " + discount_total);
                                    }
                                }
                            }
                            if (status.equals("false")) {
                                if (is_tax_applied == true) {
                                    tv_totalServicePrice.setText("$ " + total);
                                } else {
                                    tv_totalServicePrice.setText("$ " + total);
                                }
                            }
                        }
                    }));
                } else {
                    Toast.makeText(mContext, arg0.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(RetrofitError arg0) {
                Toast.makeText(mContext, arg0.getMessage(), Toast.LENGTH_SHORT).show();
            }
        };
    }

    // ********** booking APi is here **********
    private CustomCallBacks<CommonModel> bookingCalling() {
        return new CustomCallBacks<CommonModel>(mContext, true) {
            @Override
            public void onSucess(CommonModel arg0, Response arg1) {
                if (arg0.getStatus() == 200) {
                    if (rb_cod.isChecked()) {
                        orderPlacedSuccessfullyDialog();
                    }
                    if (rb_online.isChecked()) {
                        /*startActivity(new Intent(mContext, PaymentActivity.class));
                        finish();*/
                    }
                } else {
                    Toast.makeText(mContext, arg0.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(RetrofitError arg0) {
                Toast.makeText(mContext, arg0.getMessage(), Toast.LENGTH_SHORT).show();
            }
        };
    }

    //  ******************* delete address dialog ********************
    private void orderPlacedSuccessfullyDialog() {
        final AlertDialog.Builder builder2 = new AlertDialog.Builder(mContext);
        View view2 = LayoutInflater.from(mContext).inflate(R.layout.popup_order_placed, null);
        TextView ok = view2.findViewById(R.id.ok);
        builder2.setView(view2);

        dialog2 = builder2.create();
        dialog2.setCancelable(false);
        dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog2.dismiss();
                startActivity(new Intent(mContext, MainActivity.class));
                //finishAffinity();
            }
        });
        dialog2.show();
    }
}