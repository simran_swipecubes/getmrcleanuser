package com.example.cleaner.adapter;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.cleaner.R;
import com.example.cleaner.activity.DetailsServiceActivity;
import com.example.cleaner.model.GetServicesModel;

import java.util.ArrayList;
import java.util.List;


public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ViewHolder> implements Filterable {
    Activity mContext;

    public static List<String> rlist = new ArrayList<>();
    List<GetServicesModel.Datum> data;
    List<GetServicesModel.Datum> productsFilterList;
    public ServiceAdapter(Activity context, List<GetServicesModel.Datum> data) {
        this.mContext = context;
        this.data = data;
    }

    @Override
    public ServiceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_services, parent, false);
        return new ServiceAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ServiceAdapter.ViewHolder holder, final int position) {

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.mr_cleaner);
        requestOptions.error(R.drawable.mr_cleaner);
        Glide.with(mContext).
                setDefaultRequestOptions(requestOptions).
                load(data.get(position).getImage()).into(holder.item_service_image);
        holder.item_service_name.setText(data.get(position).getTitle());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, DetailsServiceActivity.class);
                intent.putExtra("service_id", data.get(position).getId());
                intent.putExtra("description", data.get(position).getDescription());
                intent.putExtra("title", data.get(position).getTitle());
                intent.putExtra("starting_Price", data.get(position).getBase_price());
                intent.putExtra("methods_id", data.get(position).getMethods_id());
                intent.putExtra("unit_id", data.get(position).getUnitid());

                mContext.startActivity(intent);
                //  mContext.overridePendingTransition(R.anim.rightsideanimation, R.anim.leftoutanimation);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView item_service_image;
        TextView item_service_name;

        public ViewHolder(View itemView) {
            super(itemView);
            item_service_image = itemView.findViewById(R.id.item_service_image);
            item_service_name = itemView.findViewById(R.id.item_service_name);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final ArrayList<GetServicesModel.Datum> results = new ArrayList<GetServicesModel.Datum>();
                if (productsFilterList == null)
                    productsFilterList = data;
                if (constraint != null) {
                    if (productsFilterList != null && productsFilterList.size() > 0) {
                        for (GetServicesModel.Datum g : productsFilterList) {
                            Log.e("here a " , constraint.toString().toUpperCase());
                            if (g.getTitle().toUpperCase().contains(constraint.toString().toUpperCase()) ||
                                    g.getTitle().toLowerCase().contains(constraint.toString().toLowerCase())) {
                                results.add(g);
                            }
                        }
                    }
                    oReturn.values = results;
                }
                return oReturn;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                data = (ArrayList<GetServicesModel.Datum>) results.values;
                notifyDataSetChanged();
            }
        };
    }
}