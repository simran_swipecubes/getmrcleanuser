package com.example.cleaner.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.example.cleaner.R;
import org.json.JSONArray;


public class SuppertAdapter extends RecyclerView.Adapter<SuppertAdapter.ViewHolder> {

    Context mContext;

    public SuppertAdapter(Context context) {
        this.mContext = context;
    }

    public interface onClickListener {
        void onClick(JSONArray array, String price, String type);
    }

    @Override
    public SuppertAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_support, parent, false);
        return new SuppertAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final SuppertAdapter.ViewHolder holder, final int position) {

    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        public ViewHolder(View itemView) {
            super(itemView);


        }
    }
}

