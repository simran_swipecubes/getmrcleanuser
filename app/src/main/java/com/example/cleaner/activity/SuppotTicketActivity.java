package com.example.cleaner.activity;

import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.cleaner.R;
import com.example.cleaner.adapter.SuppertAdapter;
import com.example.cleaner.apiservice.CustomCallBacks;
import com.example.cleaner.apiservice.RestServiceConnector;
import com.example.cleaner.model.CommonModel;
import com.example.cleaner.util.SharedPref;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SuppotTicketActivity extends AppCompatActivity implements View.OnClickListener {

    private Context context = this;
    private ImageView action_bar_back;
    private TextView actionbar_title, tv_submit_ticket;
    private RecyclerView rv_support;
    private EditText et_subject, et_query;
    private SharedPref pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suppot_ticket);

        initUis();
    }

    private void initUis() {
        pref = new SharedPref(context);
        action_bar_back = findViewById(R.id.action_bar_back);
        actionbar_title = findViewById(R.id.actionbar_title);
        rv_support = findViewById(R.id.rv_support);
        et_subject = findViewById(R.id.et_subject);
        et_query = findViewById(R.id.et_query);
        tv_submit_ticket = findViewById(R.id.tv_submit_ticket);
        actionbar_title.setText(getResources().getString(R.string.support));
        action_bar_back.setOnClickListener(this);
        tv_submit_ticket.setOnClickListener(this);

        rv_support.setLayoutManager(new LinearLayoutManager(context));
        rv_support.setAdapter(new SuppertAdapter(context));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.action_bar_back:
                finish();
                break;
            case R.id.tv_submit_ticket:
                validations();
                break;
        }
    }

    private void validations() {
        if (et_subject.getText().toString().isEmpty()) {
            Toast.makeText(context, "Please enter Subject..", Toast.LENGTH_SHORT).show();
        } else if (et_query.getText().toString().isEmpty()) {
            Toast.makeText(context, "Please enter Query..", Toast.LENGTH_SHORT).show();

        } else {
            RestServiceConnector.getService().supportTicket(pref.getUSER_ID(), et_subject.getText().toString(),
                    et_query.getText().toString(), supportTicket());
        }
    }

    private CustomCallBacks<CommonModel> supportTicket() {
        return new CustomCallBacks<CommonModel>(context, true) {
            @Override
            public void onSucess(CommonModel arg0, Response arg1) {
                if (arg0.getStatus() == 200) {
                    Toast.makeText(SuppotTicketActivity.this, arg0.getMsg(), Toast.LENGTH_SHORT).show();
                    finish();

                } else {
                    Toast.makeText(SuppotTicketActivity.this, arg0.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(RetrofitError arg0) {
                Toast.makeText(SuppotTicketActivity.this, arg0.getMessage(), Toast.LENGTH_SHORT).show();

            }
        };
    }
}
