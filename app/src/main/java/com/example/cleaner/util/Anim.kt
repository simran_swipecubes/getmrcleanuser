package com.example.cleaner.util

import android.content.Context
import android.view.animation.AnimationUtils
import com.example.cleaner.R

class Anim(c: Context)  {

     val slide_left = AnimationUtils.loadAnimation(
         c,
         R.anim.slide_in_left
     )
     val slide_right = AnimationUtils.loadAnimation(
         c,
         R.anim.slide_in_right
     )
     val slide_out = AnimationUtils.loadAnimation(
         c,
         R.anim.slide_out
     )
    val animationLogo = AnimationUtils.loadAnimation(
        c,
        R.anim.logo_animation
    )
    val slide_in_bottom = AnimationUtils.loadAnimation(
        c,
        R.anim.slide_in_bottom
    )
    val fadein = AnimationUtils.loadAnimation(
        c,
        R.anim.fade_in
    )
    val fadeout = AnimationUtils.loadAnimation(
        c,
        R.anim.fade_out
    )
    val rotatein = AnimationUtils.loadAnimation(
        c,
        R.anim.rotate_in
    )
    val rotateout = AnimationUtils.loadAnimation(
        c,
        R.anim.rotate_out
    )
    val enter = AnimationUtils.loadAnimation(
        c,
        R.anim.enter
    )
    val exit = AnimationUtils.loadAnimation(
        c,
        R.anim.exit
    )

}