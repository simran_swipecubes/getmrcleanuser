package com.example.cleaner.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import com.example.cleaner.R;
import com.example.cleaner.apiservice.CustomCallBacks;
import com.example.cleaner.apiservice.RestServiceConnector;
import com.example.cleaner.model.UpdateProfileModel;
import com.example.cleaner.model.UserDetailModel;
import com.example.cleaner.util.NetworkUtil;
import com.example.cleaner.util.SharedPref;
import com.example.cleaner.util.Util;
import com.example.cleaner.util.Utility;

import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import com.zxy.tiny.Tiny;
import com.zxy.tiny.callback.FileCallback;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

import java.io.ByteArrayOutputStream;
import java.io.File;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class UserProfileActivity extends AppCompatActivity implements View.OnClickListener {

    TextView txt_submit, user_profile_name;
    EditText et_first_name, et_last_name, et_email, et_zip, et_phone, et_address, et_city, et_state;
    String first_name, last_name, email, phone, zip, city, state, address;
    Context context;
    SharedPref pref;
    ImageView img_back, img_edit_profile, user_profile_edit_pic;
    CircleImageView user_profile_profile;
    public static final int MY_PERMISSIONS_REQUEST_EXTERNAL_STORAGE = 777;
    private static final int REQUEST_WRITE_PERMISSION = 786;
    private int GALLERY = 1, CAMERA = 2;
    String path = "", userChoosenTask = "", filepath, image_url;
    TypedFile file = null;
    double lat = 0.0, lng = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        context = UserProfileActivity.this;
        inItUi();
        //version();
    }

    public void inItUi() {
        pref = new SharedPref(context);
        user_profile_name = findViewById(R.id.user_profile_name);
        img_back = findViewById(R.id.user_profile_back);
        img_edit_profile = findViewById(R.id.user_profile_edit_img);
        et_first_name = findViewById(R.id.user_profile_first_name);
        et_last_name = findViewById(R.id.user_profile_last_name);
        txt_submit = findViewById(R.id.txt_submit);
        et_email = findViewById(R.id.user_profile_email);
        et_phone = findViewById(R.id.user_profile_phone);
        et_zip = findViewById(R.id.user_profile_zip);
        et_city = findViewById(R.id.user_profile_city);
        et_state = findViewById(R.id.user_profile_state);
        et_address = findViewById(R.id.user_profile_address);
        user_profile_profile = findViewById(R.id.user_profile_profile);
        user_profile_edit_pic = findViewById(R.id.user_profile_edit_pic);
        txt_submit.setOnClickListener(this);
        img_edit_profile.setOnClickListener(this);
        img_back.setOnClickListener(this);
        user_profile_edit_pic.setOnClickListener(this);
        lat = SplashActivity.lat;
        lng = SplashActivity.lng;

        setFields();
        enableFields(false);
        requestPermission();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.user_profile_edit_img:
                txt_submit.setVisibility(View.VISIBLE);
                user_profile_edit_pic.setVisibility(View.VISIBLE);
                enableFields(true);
                break;
            case R.id.txt_submit:
                if (NetworkUtil.INSTANCE.isConnected(context)) {
                    validateField();
                }
                break;
            case R.id.user_profile_back:
                overridePendingTransition(R.anim.rightsideanimation, R.anim.leftoutanimation);
                finish();
                break;
            case R.id.user_profile_edit_pic:
                showPictureDialog();
                break;
        }
    }

    private void enableFields(boolean b) {
        et_first_name.setEnabled(b);
        et_last_name.setEnabled(b);
        et_email.setEnabled(false);
        et_phone.setEnabled(false);
        et_city.setEnabled(b);
        et_zip.setEnabled(b);
        et_state.setEnabled(b);
        et_address.setEnabled(b);
    }

    private void setFields() {
        RestServiceConnector.getService().getUserDetail(pref.getUSER_ID(), getProfile());
    }

    private void validateField() {
        first_name = et_first_name.getText().toString().trim();
        last_name = et_last_name.getText().toString().trim();
        email = et_email.getText().toString().trim();
        phone = et_phone.getText().toString().trim();
        zip = et_zip.getText().toString().trim();
        city = et_city.getText().toString().trim();
        state = et_state.getText().toString().trim();
        address = et_address.getText().toString().trim();
        if (first_name.length() < 2) {
            et_first_name.setError("Name length can't be less than 2");
            et_first_name.requestFocus();
            et_first_name.findFocus();
        } else if (last_name.length() < 2) {
            et_last_name.setError("Name length can't be less than 2");
            et_last_name.requestFocus();
            et_last_name.findFocus();
        } else if (!Util.INSTANCE.isValidEmail(email)) {
            et_email.setError("Invalid Email");
            et_email.requestFocus();
            et_email.findFocus();
        } else if (email.length() < 9) {
            et_email.setError("Invalid Number");
            et_email.requestFocus();
            et_email.findFocus();
        } else if (phone.length() < 10) {
            et_phone.setError("Invalid Phone Number");
            et_phone.requestFocus();
            et_phone.findFocus();
        } else if (address.length() < 3) {
            et_address.setError("Address length cant be less than 3");
            et_address.requestFocus();
            et_address.findFocus();
        } else if (zip.isEmpty()) {
            et_zip.setError("Please Select Address first");
            et_zip.requestFocus();
            et_zip.findFocus();
        } else if (city.length() < 3) {
            et_city.setError("Please Select Address first");
            et_city.requestFocus();
            et_city.findFocus();
        } else if (state.length() < 3) {
            et_state.setError("Please Select Address first");
            et_state.requestFocus();
            et_state.findFocus();
        } else if (image_url.equals("")) {
            if (path.equals("")) {
                Toast.makeText(context, "Please Select Profile Picture", Toast.LENGTH_SHORT).show();
            } else {
                RestServiceConnector.getService().Update(first_name, last_name, phone, zip, address, city, state,
                        pref.getUSER_ID(), file, lat + "", lng + "",
                        update());
            }
        } else {
            RestServiceConnector.getService().Update(first_name, last_name, phone, zip, address, city, state,
                    pref.getUSER_ID(), file, lat + "", lng + "",
                    update());
        }
    }

    /*/********************* Calling UpdateProfile Api *****************************/
    public CustomCallBacks<UpdateProfileModel> update() {
        return new CustomCallBacks<UpdateProfileModel>(context, true) {
            @Override
            public void onSucess(UpdateProfileModel arg0, Response arg1) {
                if (arg0.getStatus() == 200) {
                    finish();
                    overridePendingTransition(R.anim.rightsideanimation, R.anim.leftoutanimation);
                    Toast.makeText(context, arg0.getMsg(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, arg0.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(RetrofitError arg0) {
                Toast.makeText(context, arg0.getMessage(), Toast.LENGTH_SHORT).show();
            }
        };
    }

    /*/********************* Calling get Profile  Api*****************************/
    public CustomCallBacks<UserDetailModel> getProfile() {
        return new CustomCallBacks<UserDetailModel>(context, true) {
            @Override
            public void onSucess(UserDetailModel arg0, Response arg1) {
                if (arg0.getStatus() == 200) {
                    image_url = arg0.getData().getUserimg();
                    Transformation transformation = new RoundedTransformationBuilder()
                            .borderColor(Color.BLACK)
                            .borderWidthDp(0)
                            .cornerRadiusDp(50)
                            .oval(false)
                            .build();
                    if (!image_url.equals("")) {
                        Picasso.with(context)
                                .load("http://" + image_url)
                                .fit()
                                .transform(transformation)
                                .into(user_profile_profile);
                    } else {
                        user_profile_profile.setImageResource(R.drawable.place_hol);
                    }

                    et_first_name.setText(arg0.getData().getFirstname());
                    user_profile_name.setText(arg0.getData().getFirstname() + " " + arg0.getData().getLastName());
                    et_last_name.setText(arg0.getData().getLastName());
                    et_email.setText(arg0.getData().getEmail());
                    et_phone.setText(arg0.getData().getPhone());
                    et_city.setText(arg0.getData().getCity());
                    et_state.setText(arg0.getData().getState());
                    et_address.setText(arg0.getData().getAddress());
                    et_zip.setText(arg0.getData().getZip());
                } else {
                    Toast.makeText(context, arg0.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(RetrofitError arg0) {
                Toast.makeText(context, arg0.getMessage(), Toast.LENGTH_SHORT).show();
            }
        };
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ContextCompat.checkSelfPermission(UserProfileActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(UserProfileActivity.this, Manifest.permission.CAMERA)) {
                showPictureDialog();

            } else {
                ActivityCompat.requestPermissions(UserProfileActivity.this,
                        new String[]{Manifest.permission.CAMERA,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        0);
            }
        }
    }


    //************************* Select Image From Camera And Gallery ************************************
    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
        }
    }

    //*************************** Open Camera/Gallery Dialog ************************
    private void showPictureDialog() {
        final Dialog builder = new Dialog(this, R.style.Theme_CustomDialogDimEnabled);
        Button mBtnGallery, mBtnCamera, mBtnCancel;
        builder.setContentView(R.layout.customdialogsort);
        builder.setCanceledOnTouchOutside(false);
        mBtnGallery = builder.findViewById(R.id.btn_gallery);
        mBtnCamera = builder.findViewById(R.id.btn_camera);
        mBtnCancel = builder.findViewById(R.id.btn_Cancel);
        mBtnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userChoosenTask = "Choose from Gallery";
                boolean result = Utility.checkPermission(UserProfileActivity.this);
                if (result) {
                    galleryIntent();
                }
                builder.dismiss();
            }
        });
        mBtnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(UserProfileActivity.this, WRITE_EXTERNAL_STORAGE)) {
                        ActivityCompat.requestPermissions(UserProfileActivity.this, new String[]{WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_EXTERNAL_STORAGE);

                    } else {
                        ActivityCompat.requestPermissions(UserProfileActivity.this, new String[]{WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_EXTERNAL_STORAGE);
                    }
                } else {
                    userChoosenTask = "Take Photo";

                    boolean result = Utility.checkPermission(UserProfileActivity.this);
                    if (result) {
                        takePhotoFromCamera();
                    }

                }
                builder.dismiss();

            }
        });
        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder.dismiss();
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        final Uri contentURI;
        if (resultCode == RESULT_CANCELED) {
            return;
        } else if (requestCode == GALLERY) {
            if (data != null) {
                try {
                    contentURI = data.getData();
                    Bitmap bm = MediaStore.Images.Media.getBitmap(context.getContentResolver(), data.getData());
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                    byte[] byteArray = stream.toByteArray();
                    final Bitmap compressedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                    filepath = getRealPathFromURI(contentURI);
                    Tiny.FileCompressOptions options = new Tiny.FileCompressOptions();

                    Tiny.getInstance().source(contentURI).asFile().withOptions(options).compress(new FileCallback() {
                        @Override
                        public void callback(boolean isSuccess, String outfile, Throwable t) {
                            path = outfile;
                            file = new TypedFile("image/png", new File(outfile));
                            user_profile_profile.setImageBitmap(compressedBitmap);
                        }
                    });


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } else if (requestCode == CAMERA) {
            if (resultCode == RESULT_OK) {
                final Bitmap imageBitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                imageBitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                byte[] byteArray = stream.toByteArray();
                final Bitmap compressedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                Tiny.FileCompressOptions options = new Tiny.FileCompressOptions();

                Tiny.getInstance().source(imageBitmap).asFile().withOptions(options).compress(new FileCallback() {
                    @Override
                    public void callback(boolean isSuccess, String outfile, Throwable t) {
                        path = outfile;
                        file = new TypedFile("image/png", new File(outfile));
                        user_profile_profile.setImageBitmap(compressedBitmap);
                    }
                });
            }
        }
    }
}
