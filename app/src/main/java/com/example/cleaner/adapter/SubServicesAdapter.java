package com.example.cleaner.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.example.cleaner.R;
import com.example.cleaner.model.GetSubServicesModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SubServicesAdapter extends RecyclerView.Adapter<SubServicesAdapter.ViewHolder> {
    Context mContext;
    public static List<String> rlist = new ArrayList<>();
    List<GetSubServicesModel.Datum> data;
    JSONArray array = new JSONArray();
    onClickListener onClickListener;
    int newPosition;

    public SubServicesAdapter(Context mContext, List<GetSubServicesModel.Datum> data, onClickListener onClickListener) {
        this.mContext = mContext;
        this.data = data;
        this.onClickListener = onClickListener;
    }

    public interface onClickListener {
        void onClick(JSONArray array, String price, String type);
    }

    @Override
    public SubServicesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_subservices, parent, false);
        return new SubServicesAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final SubServicesAdapter.ViewHolder holder, final int position) {
        holder.tv_name.setText(data.get(position).getAddonServiceName());
        holder.tv_price.setText("$  " + data.get(position).getBasePrice());
        final JSONObject object = new JSONObject();
        holder.tv_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newPosition = position;
                Log.e("position", newPosition + "");
                holder.tv_add.setVisibility(View.GONE);
                holder.tv_remove.setVisibility(View.VISIBLE);

                try {
                    object.put("id", data.get(newPosition).getId());
                    object.put("base_price", data.get(newPosition).getBasePrice());
                    array.put(object);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                onClickListener.onClick(array, data.get(newPosition).getBasePrice(), "add");
            }
        });

        holder.tv_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newPosition = position;
                Log.e("position", newPosition + "");

                holder.tv_remove.setVisibility(View.GONE);
                holder.tv_add.setVisibility(View.VISIBLE);

                for (int i = 0; i < array.length(); i++) {
                    JSONObject obj = null;
                    try {
                        obj = array.getJSONObject(i);
                        if (obj.getString("id").equals(data.get(newPosition).getId())) {
                            array.remove(i);
                            notifyDataSetChanged();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                onClickListener.onClick(array, data.get(newPosition).getBasePrice(), "substract");
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_name, tv_price, tv_add, tv_remove;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_price = itemView.findViewById(R.id.tv_price);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_add = itemView.findViewById(R.id.tv_add);
            tv_remove = itemView.findViewById(R.id.tv_remove);
        }
    }

}