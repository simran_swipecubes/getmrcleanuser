package com.example.cleaner.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import com.example.cleaner.R;
import com.example.cleaner.common.AppController;
import com.example.cleaner.common.GpsTracker;
import com.example.cleaner.common.PrefStore;
import com.example.cleaner.common.SingleShortLocationProvider;
import com.example.cleaner.util.Anim;
import com.example.cleaner.util.NetworkUtil;
import com.example.cleaner.util.SharedPref;
import com.example.cleaner.util.Util;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.*;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.List;
import java.util.Locale;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class SplashActivity extends AppCompatActivity {
    SharedPref pref;
    LinearLayout ll_splash;
    ImageView img;
    Anim anim;
    Handler handler;
    boolean statusOfGPS;
    public static double lat = 0, lng = 0;
    private GoogleApiClient googleApiClient;
    final static int REQUEST_LOCATION = 199;
    private static final int PERMISSION_REQUEST_CODE = 100;
    public static String city, pinCode, address = "", strAdd = "", state;
    Float loc, lnge;
    PrefStore prefStore;
    Context mContext = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        inItUi();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_LOCATION) {
            Log.d("TAG", "mmmmmmmm");
            if (resultCode == Activity.RESULT_OK) {
                getLocation();
            } else {
                finish();
            }
        }
    }

    public void inItUi() {
        prefStore = new PrefStore(mContext);
        Util.INSTANCE.fulscr(this);
        anim = new Anim(SplashActivity.this);
        img = findViewById(R.id.splash_img1);
        ll_splash = findViewById(R.id.ll_splash);
        img.startAnimation(anim.getAnimationLogo());
        ll_splash.startAnimation(anim.getSlide_in_bottom());
        pref = new SharedPref(this);

        try {
            LocationManager manager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
            statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (!statusOfGPS) {
                enableLoc();
            } else {
                requestPermission();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // checkLogin();
        FirebaseApp.initializeApp(this);
        if (NetworkUtil.INSTANCE.isConnected(this)) {
            getFirebaseToken();
        }
    }

    public void getFirebaseToken() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this,
                new OnSuccessListener<InstanceIdResult>() {
                    @Override
                    public void onSuccess(InstanceIdResult instanceIdResult) {
                        String newToken = instanceIdResult.getToken();
                        pref.setToken(newToken);
                    }
                });
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void requestPermission() {
        if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
        } else {
            if (!AppController.isNetworkAvailable(SplashActivity.this)) {
                networkDialog();
            } else {
                SingleShortLocationProvider.requestSingleUpdate(this,
                        new SingleShortLocationProvider.LocationCallback() {
                            @Override
                            public void onNewLocationAvailable(SingleShortLocationProvider.GPSCoordinates location) {
                                loc = location.latitude;
                                lnge = location.longitude;
                                getLocation();
                                checkLogin();
                            }
                        });
            }
        }
        // getLocation();
        //checkLogin();
    }

    public void checkLogin() {
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (pref.getUSER_ID().isEmpty() ) {
                    Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    overridePendingTransition(R.anim.rightsideanimation, R.anim.leftoutanimation);
                    startActivity(i);
                    finish();
                } else {
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    overridePendingTransition(R.anim.rightsideanimation, R.anim.leftoutanimation);
                    finish();
                }
            }
        }, 3000);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
        handler.removeCallbacksAndMessages(null);
    }

    // ****************************** Get User CurrentLocation *****************************
    public void getLocation() {
        GpsTracker tracker = new GpsTracker(this);
        Location location = tracker.getLocation();
        if (location == null) {
            lng = lnge;
            lat = loc;
            prefStore.saveString("latitude", lat + "");
            prefStore.saveString("longitude", lng + "");
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            try {
                List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
                if (addresses != null) {
                    Address returnedAddress = addresses.get(0);
                    address = addresses.get(0).getAddressLine(0);
                    if (addresses != null && addresses.size() > 0) {
                        city = addresses.get(0).getLocality();
                        pinCode = addresses.get(0).getPostalCode();
                        state = addresses.get(0).getAdminArea();
                        String country = addresses.get(0).getCountryName();
                    }
                    city = addresses.get(0).getLocality();
                    state = addresses.get(0).getAdminArea();
                    pinCode = addresses.get(0).getPostalCode();
                    StringBuilder strReturnedAddress = new StringBuilder("");
                    for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                        strReturnedAddress.append("Name: " + returnedAddress.getLocality() + "\n").
                                append("Sub-Admin Areas: " + returnedAddress.getSubAdminArea() + "\n").
                                append("Admin Area: " + returnedAddress.getAdminArea() + "\n").
                                append("Country: " + returnedAddress.getCountryName() + "\n").
                                append("Country Code: " + returnedAddress.getCountryCode() + "\n");
                    }
                    strAdd = strReturnedAddress.toString();
                    Log.w("My Current location", strReturnedAddress.toString());
                    checkLogin();
                } else {
                    Log.w("" +
                            "" +
                            "" +
                            " Current location", "No Address returned!");
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.w("My Current location", "Cant get Address!");
            }

        } else {
            Log.d("Location manager", "location null");
            if (location != null) {
                lng = location.getLongitude();
                lat = location.getLatitude();
                Geocoder geocoder = new Geocoder(this, Locale.getDefault());
                try {
                    List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
                    if (addresses != null) {
                        Address returnedAddress = addresses.get(0);
                        address = addresses.get(0).getAddressLine(0);
                        if (addresses != null && addresses.size() > 0) {
                            city = addresses.get(0).getLocality();
                            state = addresses.get(0).getAdminArea();
                            pinCode = addresses.get(0).getPostalCode();
                            //String state = addresses.get(0).getAdminArea();
                            //String country = addresses.get(0).getCountryName();
                        }
                        city = addresses.get(0).getLocality();
                        state = addresses.get(0).getAdminArea();
                        pinCode = addresses.get(0).getPostalCode();
                        StringBuilder strReturnedAddress = new StringBuilder("");
                        for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                            strReturnedAddress.append("Name: " + returnedAddress.getLocality() + "\n").
                                    append("Sub-Admin Areas: " + returnedAddress.getSubAdminArea() + "\n").
                                    append("Admin Area: " + returnedAddress.getAdminArea() + "\n").
                                    append("Country: " + returnedAddress.getCountryName() + "\n").
                                    append("Country Code: " + returnedAddress.getCountryCode() + "\n");
                        }
                        strAdd = strReturnedAddress.toString();
                        Log.w("My Current location", strReturnedAddress.toString());
                        checkLogin();
                    } else {
                        Log.w("My Current location", "No Address returned!");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.w("My Current location", "Cant get Address!");
                }

            } else {
                Log.d("Location manager", "location null");
            }
        }
    }

    private void enableLoc() {
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(@Nullable Bundle bundle) {

                        }

                        @Override
                        public void onConnectionSuspended(int i) {

                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(ConnectionResult connectionResult) {
                            Log.d("Location error ", connectionResult.getErrorCode() + "");
                        }
                    }).build();
            googleApiClient.connect();
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);
            builder.setAlwaysShow(true);
            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(@NonNull LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                status.startResolutionForResult(SplashActivity.this, REQUEST_LOCATION);
                                //getLocation();

                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                                Toast.makeText(SplashActivity.this, "Error", Toast.LENGTH_SHORT).show();
                            }
                            break;
                    }
                }
            });
        }
    }

    // ***************************** Network Dialog ******************************
    public void networkDialog() {
        Dialog alertDialog = new Dialog(this);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.popup_network);
        TextView ok = alertDialog.findViewById(R.id.ok);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.FadeOutAnimation;
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
    }

}
