package com.example.cleaner.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HistoryModel {


    public class Datum {

        @SerializedName("rating")
        @Expose
        private String rating;
        @SerializedName("amount")
        @Expose
        private String amount;
        @SerializedName("staff_ids")
        @Expose
        private String staffIds;
        @SerializedName("booking_date_time")
        @Expose
        private String bookingDateTime;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("booking_status")
        @Expose
        private String bookingStatus;
        @SerializedName("order_id")
        @Expose
        private String orderId;
        @SerializedName("vendor")
        @Expose
        private String vendor;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("client_personal_info")
        @Expose
        private String clientPersonalInfo;
        @SerializedName("net_amount")
        @Expose
        private String netAmount;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("path")
        @Expose
        private String path;

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getStaffIds() {
            return staffIds;
        }

        public void setStaffIds(String staffIds) {
            this.staffIds = staffIds;
        }

        public String getBookingDateTime() {
            return bookingDateTime;
        }

        public void setBookingDateTime(String bookingDateTime) {
            this.bookingDateTime = bookingDateTime;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getBookingStatus() {
            return bookingStatus;
        }

        public void setBookingStatus(String bookingStatus) {
            this.bookingStatus = bookingStatus;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getVendor() {
            return vendor;
        }

        public void setVendor(String vendor) {
            this.vendor = vendor;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getClientPersonalInfo() {
            return clientPersonalInfo;
        }

        public void setClientPersonalInfo(String clientPersonalInfo) {
            this.clientPersonalInfo = clientPersonalInfo;
        }

        public String getNetAmount() {
            return netAmount;
        }

        public void setNetAmount(String netAmount) {
            this.netAmount = netAmount;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

    }


        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("data")
        @Expose
        private List<Datum> data = null;
        @SerializedName("msg")
        @Expose
        private String msg;

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public List<Datum> getData() {
            return data;
        }

        public void setData(List<Datum> data) {
            this.data = data;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }


}
