package com.example.cleaner.apiservice;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

import java.util.concurrent.TimeUnit;

public class RestServiceConnector {
    private static RestApi service;

    //service before login
    public static RestApi getService() {
        return (service == null) ? setService() : service;
    }

    private static RestApi setService() {
        OkHttpClient http = new OkHttpClient();
        http.setReadTimeout(30000, TimeUnit.MILLISECONDS);
        http.setWriteTimeout(30000, TimeUnit.MILLISECONDS);
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        RestAdapter restAdapter = new RestAdapter.Builder().
                setEndpoint(ApiConstants.BaseURL)
                .setRequestInterceptor(interceptor)
                .setConverter(new LenientGsonConverter(gson))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setClient(new OkClient(http)).build();
        service = restAdapter.create(RestApi.class);
        return service;
    }

    private static RequestInterceptor interceptor = new RequestInterceptor() {
        @Override
        public void intercept(RequestFacade arg0) {
            arg0.addHeader(ApiConstants.HEADER_TOKEN, ApiConstants.COMMON_HEADER_TOKEN);

        }
    };


}

