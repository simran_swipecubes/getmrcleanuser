package com.example.cleaner.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import androidx.recyclerview.widget.RecyclerView;
import com.example.cleaner.R;

import com.example.cleaner.model.HistoryModel;
import com.example.cleaner.util.SharedPref;

import java.util.List;

public class PaymentsListAdapter extends RecyclerView.Adapter<PaymentsListAdapter.ViewHolder> {
    Context mContext;
    SharedPref pref;
    List<HistoryModel.Datum> data;
    AlertDialog dialog2;

    public PaymentsListAdapter(Context mContext) {
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public PaymentsListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_history, parent, false);
        pref = new SharedPref(mContext);
        return new PaymentsListAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final PaymentsListAdapter.ViewHolder holder, final int position) {

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView tv_title, tv_price, tv_date, tv_status, tv_vendor_name, tv_address;
        RelativeLayout rl_rate;
        RatingBar rating_bar;

        public ViewHolder(View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.item_service_image);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_price = itemView.findViewById(R.id.tv_price);
            tv_date = itemView.findViewById(R.id.tv_date);
            tv_status = itemView.findViewById(R.id.tv_status);
            tv_vendor_name = itemView.findViewById(R.id.tv_vendor_name);
            tv_address = itemView.findViewById(R.id.tv_address);
            rl_rate = itemView.findViewById(R.id.rl_rate);
            rating_bar = itemView.findViewById(R.id.rating_bar);
        }
    }

}

