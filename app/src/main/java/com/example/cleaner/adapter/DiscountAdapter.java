package com.example.cleaner.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.example.cleaner.R;
import com.example.cleaner.model.GetAddressModel;

import java.util.List;

public class DiscountAdapter extends RecyclerView.Adapter<DiscountAdapter.ViewHolder> {
    private Context mContext;
    List<GetAddressModel.Discount> discount;
    onClickListener onClickListener;
    private int selectedPosition = -1;

    public interface onClickListener {
        void onClick(String rates, String dType, String aFalse);
    }

    public DiscountAdapter(Context mContext, List<GetAddressModel.Discount> discount, onClickListener onClickListener) {
        this.mContext = mContext;
        this.discount = discount;
        this.onClickListener = onClickListener;
    }

    @Override
    public DiscountAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_discount, parent, false);
        return new DiscountAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final DiscountAdapter.ViewHolder holder, final int position) {
        holder.tv_name.setText(discount.get(position).getLabels());
        holder.cb_discount.setText(discount.get(position).getDiscountTypename());

        holder.cb_discount.setChecked(position == selectedPosition);

        holder.cb_discount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position == selectedPosition) {
                    holder.cb_discount.setChecked(false);
                    // false means rate not added
                    onClickListener.onClick(discount.get(position).getRates(), discount.get(position).getDType(), "false");
                } else {
                    // true means rate not added
                    selectedPosition = position;
                    notifyDataSetChanged();
                    onClickListener.onClick(discount.get(position).getRates(), discount.get(position).getDType(), "true");
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return discount.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_name;
        private RelativeLayout rl_discount;
        CheckBox cb_discount;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            rl_discount = itemView.findViewById(R.id.rl_discount);
            cb_discount = itemView.findViewById(R.id.cb_discount);
        }
    }


    //  ******************* delete address dialog ********************
    private void deleteProductDialog(final int pos) {
        final AlertDialog.Builder builder2 = new AlertDialog.Builder(mContext);
        View view2 = LayoutInflater.from(mContext).inflate(R.layout.popup_delete, null);
        TextView cancel = view2.findViewById(R.id.cancel);
        TextView ok = view2.findViewById(R.id.ok);
        builder2.setView(view2);
        final AlertDialog dialog2 = builder2.create();
        dialog2.setCancelable(false);
        dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog2.dismiss();
               /* onClickListener.onClick(data.get(pos).getId(), pos);
                data.remove(pos);
                notifyDataSetChanged();*/

            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog2.dismiss();
            }
        });
        dialog2.show();
    }

}
