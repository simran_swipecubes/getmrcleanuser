package com.example.cleaner.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.cleaner.R;
import com.example.cleaner.adapter.ServiceAdapter;
import com.example.cleaner.apiservice.CustomCallBacks;
import com.example.cleaner.apiservice.RestServiceConnector;
import com.example.cleaner.fragment.NavFragment;
import com.example.cleaner.model.GetServicesModel;
import com.example.cleaner.model.UserDetailModel;
import com.example.cleaner.util.SharedPref;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    Context context;
    RecyclerView rv;
    TextView tv_services;
    ArrayList<String> service_list;
    GridLayoutManager gm;
    public static DrawerLayout drawerLayout;
    ImageView img_nav;
    SharedPref pref;
    EditText et_serach;
    ServiceAdapter serviceAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Util.INSTANCE.fulscr(this);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;
        pref = new SharedPref(context);
    }

    private void InitUis() {
        drawerLayout = findViewById(R.id.drawer_layout);
        img_nav = findViewById(R.id.nav_img);
        et_serach = findViewById(R.id.et_serach);
        tv_services = findViewById(R.id.tv_services);
        rv = findViewById(R.id.rv);
        service_list = new ArrayList<>();

        FragmentTransaction fragementTransaction = getSupportFragmentManager().beginTransaction();
        fragementTransaction.replace(R.id.side_frame1, new NavFragment());
        fragementTransaction.commit();
        img_nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.openDrawer(GravityCompat.START);
                    Log.d("TAG", "if");
                } else {
                    drawerLayout.closeDrawers();
                    Log.d("TAG", "else");
                }
            }
        });
        searchServices();
    }

    public CustomCallBacks<GetServicesModel> ServicesListCallback() {
        return new CustomCallBacks<GetServicesModel>(context, true) {
            @Override
            public void onSucess(GetServicesModel arg0, Response arg1) {
                if (arg0.getStatus() == 200) {
                    Log.e("TAG", "success");
                    gm = new GridLayoutManager(context, 2);
                    rv.setLayoutManager(gm);
                    tv_services.setText(arg0.getTagline());
                    serviceAdapter = new ServiceAdapter(MainActivity.this, arg0.getData());
                    rv.setAdapter(serviceAdapter);
                    RestServiceConnector.getService().getUserDetail(pref.getUSER_ID(), getProfile());
                } else {
                    Log.e("TAG", "profile else");
                    Toast.makeText(context, arg0.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(RetrofitError arg0) {
                Log.e("TAG", "profile fail");
                Toast.makeText(context, arg0.getMessage(), Toast.LENGTH_SHORT).show();
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("TAG", "resume");
        if (pref.getUSER_ID().isEmpty()) {
            Intent i = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(i);
        } else {
            InitUis();
            RestServiceConnector.getService().getServicesList(ServicesListCallback());
        }
    }

    //  ********************** Calling get Profile  Api*****************************/
    public CustomCallBacks<UserDetailModel> getProfile() {
        return new CustomCallBacks<UserDetailModel>(context, false) {
            @Override
            public void onSucess(UserDetailModel arg0, Response arg1) {
                if (arg0.getStatus() == 200) {
                    Log.e("TAG", "profile");
                    RequestOptions requestOptions = new RequestOptions();
                    requestOptions.placeholder(R.drawable.ic_user_profile);
                    requestOptions.error(R.drawable.ic_user_profile);
                    Glide.with(context).
                            setDefaultRequestOptions(requestOptions).
                            load("http://" + arg0.getData().getUserimg()).into(NavFragment.nav_profile);
                    NavFragment.nav_name.setText(arg0.getData().getFirstname() + " " + arg0.getData().getLastName());
                    NavFragment.nav_email.setText(arg0.getData().getEmail());
                } else {
                    Log.e("TAG", "profile else");
                    Toast.makeText(context, arg0.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(RetrofitError arg0) {
                Log.e("TAG", "profile fail");
                Toast.makeText(context, arg0.getMessage(), Toast.LENGTH_SHORT).show();
            }
        };
    }

    private void searchServices() {
        et_serach.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                serviceAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        exit_Dialog();
    }

    //  ******************* delete address dialog ********************
    private void exit_Dialog() {
        final AlertDialog.Builder builder2 = new AlertDialog.Builder(context);
        View view2 = LayoutInflater.from(context).inflate(R.layout.popup_exit, null);
        TextView cancel = view2.findViewById(R.id.cancel);
        TextView ok = view2.findViewById(R.id.ok);
        builder2.setView(view2);
        final AlertDialog dialog2 = builder2.create();
        dialog2.setCancelable(false);
        dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog2.dismiss();
                finishAffinity();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog2.dismiss();
            }
        });
        dialog2.show();
    }

}
