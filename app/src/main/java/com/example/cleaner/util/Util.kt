package com.example.cleaner.util

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Context.CLIPBOARD_SERVICE
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import java.net.URL
import javax.net.ssl.HttpsURLConnection

object Util {
    fun isValidEmail(target: CharSequence?): Boolean {
        return if (target == null) {
            false
        } else {
            android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }
    }

    fun fulscr(appCompatActivity: AppCompatActivity) {
        appCompatActivity.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }

    fun clickablefalse(appCompatActivity: AppCompatActivity) {
        appCompatActivity.window.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
        )
    }

    fun clickabletrue(appCompatActivity: AppCompatActivity) {
        appCompatActivity.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }

    fun toast(context: Context, msg: String) {
        Toast.makeText(
            context, "" + msg,
            Toast.LENGTH_SHORT
        ).show()
    }


    fun copyText(context: Context, text: String) {
        val clipboard = context.getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("label", text)
        clipboard.setPrimaryClip(clip)
    }

    fun getFacebookProfilePicture(userID: String): Bitmap? {
        var fb_img: Bitmap? = null
        try {
            val fb_url = URL("http://graph.facebook.com/$userID/picture?type=small")//small | noraml | large
            val conn1 = fb_url.openConnection() as HttpsURLConnection
            HttpsURLConnection.setFollowRedirects(true)
            conn1.instanceFollowRedirects = true
            fb_img = BitmapFactory.decodeStream(conn1.inputStream)
            return fb_img
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return fb_img
    }

    fun hideSoftKeyboard(activity: AppCompatActivity) {
        val inputMethodManager = activity.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, 0)
    }

}


