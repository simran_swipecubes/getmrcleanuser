package com.example.cleaner.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetSubServicesModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("msg")
    @Expose
    private String msg;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public class Datum {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("service_id")
        @Expose
        private String serviceId;
        @SerializedName("addon_service_name")
        @Expose
        private String addonServiceName;
        @SerializedName("base_price")
        @Expose
        private String basePrice;
        @SerializedName("maxqty")
        @Expose
        private String maxqty;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("multipleqty")
        @Expose
        private String multipleqty;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("position")
        @Expose
        private String position;
        @SerializedName("predefine_image")
        @Expose
        private String predefineImage;
        @SerializedName("predefine_image_title")
        @Expose
        private String predefineImageTitle;
        @SerializedName("aduration")
        @Expose
        private String aduration;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getServiceId() {
            return serviceId;
        }

        public void setServiceId(String serviceId) {
            this.serviceId = serviceId;
        }

        public String getAddonServiceName() {
            return addonServiceName;
        }

        public void setAddonServiceName(String addonServiceName) {
            this.addonServiceName = addonServiceName;
        }

        public String getBasePrice() {
            return basePrice;
        }

        public void setBasePrice(String basePrice) {
            this.basePrice = basePrice;
        }

        public String getMaxqty() {
            return maxqty;
        }

        public void setMaxqty(String maxqty) {
            this.maxqty = maxqty;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getMultipleqty() {
            return multipleqty;
        }

        public void setMultipleqty(String multipleqty) {
            this.multipleqty = multipleqty;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }

        public String getPredefineImage() {
            return predefineImage;
        }

        public void setPredefineImage(String predefineImage) {
            this.predefineImage = predefineImage;
        }

        public String getPredefineImageTitle() {
            return predefineImageTitle;
        }

        public void setPredefineImageTitle(String predefineImageTitle) {
            this.predefineImageTitle = predefineImageTitle;
        }

        public String getAduration() {
            return aduration;
        }

        public void setAduration(String aduration) {
            this.aduration = aduration;
        }

    }


}
