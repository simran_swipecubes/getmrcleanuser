package com.example.cleaner.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

object NetworkUtil {

    private val TYPE_CONNECTED = 1
    private val TYPE_NOT_CONNECTED = 0
    private fun getConnectivityStatus(context: Context): Int {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        var activeNetwork: NetworkInfo?
       // if (cm != null) {
            activeNetwork = cm.activeNetworkInfo
       // }
        if (null != activeNetwork) {
            if (activeNetwork.isConnected)
                return TYPE_CONNECTED

        }
        return TYPE_NOT_CONNECTED
    }

    fun isConnected(context: Context): Boolean {
        val conn = getConnectivityStatus(context)
        var status = false
        if (conn == TYPE_CONNECTED) {
            status = true
        } else if (conn == TYPE_NOT_CONNECTED) {
            Util.toast(context, "No Internet Connection")
            status = false
        }
        return status
    }

    fun isConnectedBroadCast(context: Context): Boolean {
        val conn = getConnectivityStatus(context)
        var status = false
        if (conn == TYPE_CONNECTED) {
            status = true
        } else if (conn == TYPE_NOT_CONNECTED) {
            status = false
        }
        return status
    }


}
