package com.example.cleaner.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.example.cleaner.R;
import com.example.cleaner.apiservice.CustomCallBacks;
import com.example.cleaner.apiservice.RestServiceConnector;
import com.example.cleaner.common.PrefStore;
import com.example.cleaner.model.CommonModel;
import com.example.cleaner.model.EditAddressModel;
import com.example.cleaner.util.SharedPref;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.HashMap;
import java.util.Map;

public class AddAddressActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView actionbar_title, tv_cancel, tv_save;
    private ImageView action_bar_back;
    private EditText et_zip, et_city, et_state, et_address;
    SharedPref pref;
    Context mContext = this;
    Intent intent;
    String address, lat = "", lng = "", zip, city, state, mapActivity, address_id;
    PrefStore prefStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);
        initUi();
    }

    private void initUi() {
        intent = getIntent();
        pref = new SharedPref(mContext);
        prefStore = new PrefStore(mContext);
        lat = String.valueOf(SplashActivity.lat);
        lng = String.valueOf(SplashActivity.lng);
        actionbar_title = findViewById(R.id.actionbar_title);
        action_bar_back = findViewById(R.id.action_bar_back);
        tv_cancel = findViewById(R.id.tv_cancel);
        tv_save = findViewById(R.id.tv_save);
        et_zip = findViewById(R.id.et_zip);
        et_city = findViewById(R.id.et_city);
        et_state = findViewById(R.id.et_state);
        et_address = findViewById(R.id.et_address);
        et_address.setSelected(true);
        if (intent.getStringExtra("where").equals("edit")) {
            actionbar_title.setText("Edit Address");
            tv_save.setText("Update Address");
            address_id = intent.getStringExtra("address_id");
            Map<String, String> data = new HashMap<>();
            data.put("uid", pref.getUSER_ID());
            data.put("aid", address_id);
            RestServiceConnector.getService().editAddress(data, editAddressCallback());
        }
        if (intent.getStringExtra("where").equals("my_address")) {
            actionbar_title.setText("Add Address");
            tv_save.setText("Save Address");
        }
        if (intent.getStringExtra("where").equals("booking")) {
            actionbar_title.setText("Add Address");
            tv_save.setText("Save Address");
        }
        //et_address.setFocusable(false);
        et_zip.setEnabled(false);
        et_city.setEnabled(false);
        et_state.setEnabled(false);
        action_bar_back.setOnClickListener(this);
        tv_cancel.setOnClickListener(this);
        tv_save.setOnClickListener(this);
        et_address.setOnClickListener(this);
        /*if (SplashCopyActivity.address == null) {
            et_address.setText(address);
            et_zip.setText(zip);
            et_state.setText(state);
            et_city.setText(city);
        } else {
            et_address.setText(SplashCopyActivity.address);
            et_zip.setText(SplashCopyActivity.postalCode);
            et_state.setText(SplashCopyActivity.state);
            et_city.setText(SplashCopyActivity.city);
        }*/
    }

    private void validations() {
        if (et_address.getText().toString().length() < 3) {
            et_address.setError("Address length cant be less than 3");
            et_address.requestFocus();
            et_address.findFocus();
        } else if (et_zip.getText().toString().isEmpty()) {
            et_zip.setError("Please select Address first");
            et_zip.requestFocus();
            et_zip.findFocus();
        } else if (et_city.getText().toString().length() < 3) {
            et_city.setError("Please select Address first");
            et_city.requestFocus();
            et_city.findFocus();
        } else if (et_state.getText().toString().length() < 3) {
            et_state.setError("Please select Address first");
            et_state.requestFocus();
            et_state.findFocus();
        } else {
            if (intent.getStringExtra("where").equals("my_address")) {
                RestServiceConnector.getService().addAddress(pref.getUSER_ID(), et_zip.getText().toString(),
                        et_city.getText().toString(), et_state.getText().toString(), lat, lng,
                        et_address.getText().toString(), addAddressCallbacks());
            }
            if (intent.getStringExtra("where").equals("booking")) {
                RestServiceConnector.getService().addAddress(pref.getUSER_ID(), et_zip.getText().toString(),
                        et_city.getText().toString(), et_state.getText().toString(), lat, lng,
                        et_address.getText().toString(), addAddressCallbacks());
            }
            if (intent.getStringExtra("where").equals("edit")) {
                if (lat.equals("") && lng.equals("")) {
                    RestServiceConnector.getService().updateAddress(address_id, et_zip.getText().toString(),
                            et_city.getText().toString(), et_state.getText().toString(),
                            et_address.getText().toString(), SplashActivity.lat + "",
                            SplashActivity.lng + "",
                            updateAddress());
                } else {
                    RestServiceConnector.getService().updateAddress(address_id, et_zip.getText().toString(),
                            et_city.getText().toString(), et_state.getText().toString(),
                            et_address.getText().toString(), lat, lng, updateAddress());
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            return;
        }
        if (requestCode == 12) {
            address = data.getStringExtra("txt_location");
            lat = data.getStringExtra("lat");
            lng = data.getStringExtra("lng");
            zip = data.getStringExtra("pincode");
            state = data.getStringExtra("state");
            city = data.getStringExtra("city");
            mapActivity = data.getStringExtra("mapActivity");
            et_address.setText(address);
            et_zip.setText(zip);
            et_state.setText(state);
            et_city.setText(city);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.action_bar_back:
                onBackPressed();
                break;
            case R.id.tv_cancel:
                et_zip.setText("");
                et_city.setText("");
                et_state.setText("");
                et_address.setText("");
                break;
            case R.id.tv_save:
                validations();
                break;
            case R.id.et_address:
                et_address.setFocusable(true);
                Intent i = new Intent(AddAddressActivity.this, MapActivity.class).
                        putExtra("type", "address");
                startActivityForResult(i, 12);
                break;
        }
    }

    // ************** edit Address Api is here *************
    private CustomCallBacks<EditAddressModel> editAddressCallback() {
        return new CustomCallBacks<EditAddressModel>(mContext, true) {
            @Override
            public void onSucess(EditAddressModel arg0, Response arg1) {
                if (arg0.getStatus() == 200) {
                    et_zip.setText(arg0.getData().get(0).getZip());
                    et_city.setText(arg0.getData().get(0).getCity());
                    et_state.setText(arg0.getData().get(0).getState());
                    et_address.setText(arg0.getData().get(0).getAddress());
                } else {
                    Toast.makeText(mContext, arg0.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(RetrofitError arg0) {
                Toast.makeText(mContext, arg0.getMessage(), Toast.LENGTH_SHORT).show();
            }
        };
    }

    // *********** Add Adddress APi is here *********
    private CustomCallBacks<CommonModel> addAddressCallbacks() {
        return new CustomCallBacks<CommonModel>(this, true) {
            @Override
            public void onSucess(CommonModel arg0, Response arg1) {
                if (arg0.getStatus() == 200) {
                    if (intent.getStringExtra("where").equals("my_address")) {
                        Toast.makeText(AddAddressActivity.this, arg0.getMsg(), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    if (intent.getStringExtra("where").equals("booking")) {
                        startActivity(new Intent(AddAddressActivity.this, MainActivity.class));
                        finish();
                    }
                } else {
                    Toast.makeText(AddAddressActivity.this, arg0.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(RetrofitError arg0) {
                Toast.makeText(AddAddressActivity.this, arg0.getMessage(), Toast.LENGTH_SHORT).show();

            }
        };
    }

    // *********** Update Address Api is here ***************
    private CustomCallBacks<CommonModel> updateAddress() {
        return new CustomCallBacks<CommonModel>(mContext, true) {
            @Override
            public void onSucess(CommonModel arg0, Response arg1) {
                if (arg0.getStatus() == 200) {
                    Toast.makeText(mContext, arg0.getMsg(), Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(mContext, arg0.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(RetrofitError arg0) {
                Toast.makeText(mContext, arg0.getMessage(), Toast.LENGTH_SHORT).show();

            }
        };
    }
}
