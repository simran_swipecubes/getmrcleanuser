package com.example.cleaner.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetAddressModel {

    public class Datum {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("uid")
        @Expose
        private String uid;
        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("state")
        @Expose
        private String state;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("zip")
        @Expose
        private String zip;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("is_delete")
        @Expose
        private String isDelete;
        @SerializedName("defaultaddr")
        @Expose
        private String defaultaddr;
        @SerializedName("lat")
        @Expose
        private String lat;
        @SerializedName("lng")
        @Expose
        private String lng;
        @SerializedName("first_name")
        @Expose
        private String firstName;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getZip() {
            return zip;
        }

        public void setZip(String zip) {
            this.zip = zip;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getIsDelete() {
            return isDelete;
        }

        public void setIsDelete(String isDelete) {
            this.isDelete = isDelete;
        }

        public String getDefaultaddr() {
            return defaultaddr;
        }

        public void setDefaultaddr(String defaultaddr) {
            this.defaultaddr = defaultaddr;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

    }

    public class Discount {
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("discount_typename")
        @Expose
        private String discountTypename;
        @SerializedName("d_type")
        @Expose
        private String dType;
        @SerializedName("rates")
        @Expose
        private String rates;
        @SerializedName("labels")
        @Expose
        private String labels;
        @SerializedName("status")
        @Expose
        private String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDiscountTypename() {
            return discountTypename;
        }

        public void setDiscountTypename(String discountTypename) {
            this.discountTypename = discountTypename;
        }

        public String getDType() {
            return dType;
        }

        public void setDType(String dType) {
            this.dType = dType;
        }

        public String getRates() {
            return rates;
        }

        public void setRates(String rates) {
            this.rates = rates;
        }

        public String getLabels() {
            return labels;
        }

        public void setLabels(String labels) {
            this.labels = labels;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }


        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("tax")
        @Expose
        private Tax tax;
        @SerializedName("piece")
        @Expose
        private List<Piece> piece = null;
        @SerializedName("discount")
        @Expose
        private List<Discount> discount = null;
        @SerializedName("data")
        @Expose
        private List<Datum> data = null;
        @SerializedName("msg")
        @Expose
        private String msg;

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public Tax getTax() {
            return tax;
        }

        public void setTax(Tax tax) {
            this.tax = tax;
        }

        public List<Piece> getPiece() {
            return piece;
        }

        public void setPiece(List<Piece> piece) {
            this.piece = piece;
        }

        public List<Discount> getDiscount() {
            return discount;
        }

        public void setDiscount(List<Discount> discount) {
            this.discount = discount;
        }

        public List<Datum> getData() {
            return data;
        }

        public void setData(List<Datum> data) {
            this.data = data;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }




    public class Piece {

        @SerializedName("minlimit")
        @Expose
        private String minlimit;
        @SerializedName("maxlimit")
        @Expose
        private String maxlimit;

        public String getMinlimit() {
            return minlimit;
        }

        public void setMinlimit(String minlimit) {
            this.minlimit = minlimit;
        }

        public String getMaxlimit() {
            return maxlimit;
        }

        public void setMaxlimit(String maxlimit) {
            this.maxlimit = maxlimit;
        }

    }


    public class Tax {

        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("value")
        @Expose
        private String value;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }

}
