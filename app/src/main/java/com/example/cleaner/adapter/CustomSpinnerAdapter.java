package com.example.cleaner.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.example.cleaner.R;

public class CustomSpinnerAdapter extends BaseAdapter {
    Context context;
    String[] pieces;
    LayoutInflater inflter;

    public CustomSpinnerAdapter(Context applicationContext, String[] pieces) {
        this.context = applicationContext;
        this.pieces = pieces;

        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return pieces.length;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.items_spinner, null);

        TextView tv_pieces = view.findViewById(R.id.tv_pieces);
        tv_pieces.setText(pieces[i]);
        return view;
    }
}