package com.example.cleaner.common;

import com.stripe.android.model.Card;

/**
 * Created by user5 on 12/6/17.
 */

public interface OnPayBtnClickListner {
    public void onClick(Card card);
}