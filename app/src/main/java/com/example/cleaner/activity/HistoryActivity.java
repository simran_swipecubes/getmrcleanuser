package com.example.cleaner.activity;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.cleaner.R;
import com.example.cleaner.adapter.HistoryAdapter;
import com.example.cleaner.apiservice.CustomCallBacks;
import com.example.cleaner.apiservice.RestServiceConnector;
import com.example.cleaner.model.HistoryModel;
import com.example.cleaner.util.SharedPref;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.HashMap;
import java.util.Map;

public class HistoryActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView rv_history;
    Context mContext = this;
    TextView tv_no_history;
    TextView actionbar_title;
    ImageView action_bar_back;
    SharedPref pref;
    boolean type = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        initUis();
    }

    private void initUis() {
        pref = new SharedPref(this);
        rv_history = findViewById(R.id.rv_history);
        tv_no_history = findViewById(R.id.tv_no_history);
        action_bar_back = findViewById(R.id.action_bar_back);
        actionbar_title = findViewById(R.id.actionbar_title);
        Map<String, String> data = new HashMap<>();
        data.put("client_id", pref.getUSER_ID());
        actionbar_title.setText(getResources().getString(R.string.history));
        type = true;
        RestServiceConnector.getService().history(data, historyCallbacks());

        action_bar_back.setOnClickListener(this);
    }

    // ************** history APi is here ************
    private CustomCallBacks<HistoryModel> historyCallbacks() {
        return new CustomCallBacks<HistoryModel>(mContext, type) {
            @Override
            public void onSucess(HistoryModel arg0, Response arg1) {
                if (arg0.getStatus() == 200) {
                    tv_no_history.setVisibility(View.GONE);
                    rv_history.setLayoutManager(new LinearLayoutManager(mContext));
                    rv_history.setAdapter(new HistoryAdapter(mContext, arg0.getData(), new HistoryAdapter.onClickListerner() {
                        @Override
                        public void onClick() {
                            Map<String, String> data = new HashMap<>();
                            data.put("client_id", pref.getUSER_ID());
                            type = false;
                            RestServiceConnector.getService().history(data, historyCallbacks());
                        }
                    }));
                } else {
                    tv_no_history.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(RetrofitError arg0) {
                Toast.makeText(mContext, arg0.getMessage(), Toast.LENGTH_SHORT).show();
            }
        };
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.action_bar_back:
                finish();
                break;
        }
    }


}
