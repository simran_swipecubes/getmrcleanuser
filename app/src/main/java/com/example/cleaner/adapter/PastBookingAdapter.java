package com.example.cleaner.adapter;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import androidx.recyclerview.widget.RecyclerView;
import com.example.cleaner.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


public class PastBookingAdapter extends RecyclerView.Adapter<PastBookingAdapter.ViewHolder> {
    Context mContext;
    List<HashMap<String, String>> data;
    onClickListener onClickListener;
    EditText dateEdittext, tv_time;
    TimePicker simpleTimePicker;
    String  time = "", datesss;

    public PastBookingAdapter(Context mContext, List<HashMap<String, String>> data, String datesss,
                              PastBookingAdapter.onClickListener onClickListener) {
        this.mContext = mContext;
        this.data = data;
        this.onClickListener = onClickListener;
        this.datesss = datesss;
    }

    public interface onClickListener {
        void onClick(String orderId, String s, TextView tv_cancel, TextView tv_ReSchedule);

        void onRescheduleClick(String formattedDate, String time, String orderId, AlertDialog dialog2);
    }

    @Override
    public PastBookingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_bookings, parent, false);
        return new PastBookingAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final PastBookingAdapter.ViewHolder holder, final int position) {

        String title = data.get(position).get("title");
        Log.e("TAG", title);
        holder.tv_title.setText(data.get(position).get("title"));
        holder.tv_price.setText("$ " + data.get(position).get("price"));
        //holder.tv_date.setText(data.get(position).getBookingDateTime());
        holder.tv_status.setText(data.get(position).get("status"));
        holder.tv_address.setText(data.get(position).get("address"));
        holder.tv_vendor_name.setText(data.get(position).get("vendor_name"));
        if (data.get(position).get("booking_status").equals("A")) {
            holder.tv_cancel.setVisibility(View.VISIBLE);
            holder.tv_ReSchedule.setVisibility(View.VISIBLE);
        }
        if (data.get(position).get("booking_status").equals("C")) {
            holder.tv_cancel.setVisibility(View.GONE);
            holder.tv_ReSchedule.setVisibility(View.GONE);
        }
        if (data.get(position).get("booking_status").equals("CS")) {
            holder.tv_cancel.setText("Cancelled");
            holder.tv_cancel.setClickable(false);
            holder.tv_cancel.setVisibility(View.VISIBLE);
            holder.tv_ReSchedule.setVisibility(View.GONE);
        }
        if (data.get(position).get("booking_status").equals("CC")) {
            holder.tv_cancel.setText("Cancelled");
            holder.tv_cancel.setClickable(false);
            holder.tv_cancel.setVisibility(View.VISIBLE);
            holder.tv_ReSchedule.setVisibility(View.GONE);
        }
        holder.tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteOrderDialog(data.get(position).get("o_id"), holder.tv_cancel, holder.tv_ReSchedule);
            }
        });
        holder.tv_ReSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (data.get(position).get("staff_id").equals("")) {
                    AssignedOrderDialog();
                } else {
                    ScheduleOrderDialog(data.get(position).get("o_id"));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView tv_title, tv_price, tv_date, tv_status, tv_vendor_name, tv_address, tv_cancel, tv_ReSchedule;

        public ViewHolder(View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.item_service_image);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_price = itemView.findViewById(R.id.tv_price);
            tv_date = itemView.findViewById(R.id.tv_date);
            tv_status = itemView.findViewById(R.id.tv_status);
            tv_vendor_name = itemView.findViewById(R.id.tv_vendor_name);
            tv_address = itemView.findViewById(R.id.tv_address);
            tv_cancel = itemView.findViewById(R.id.tv_cancel);
            tv_ReSchedule = itemView.findViewById(R.id.tv_ReSchedule);
        }
    }

    //  ******************* delete Order dialog ********************
    private void deleteOrderDialog(final String orderId, final TextView tv_cancel, final TextView tv_ReSchedule) {
        final AlertDialog.Builder builder2 = new AlertDialog.Builder(mContext);
        View view2 = LayoutInflater.from(mContext).inflate(R.layout.popup_cancel, null);
        TextView cancel = view2.findViewById(R.id.cancel);
        TextView ok = view2.findViewById(R.id.ok);
        final EditText et_reason = view2.findViewById(R.id.et_reason);
        builder2.setView(view2);
        final AlertDialog dialog2 = builder2.create();
        dialog2.setCancelable(false);
        dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog2.dismiss();
                if (et_reason.getText().toString().isEmpty()) {
                    Toast.makeText(mContext, "Please Enter Reason First", Toast.LENGTH_SHORT).show();
                } else {
                    onClickListener.onClick(orderId, et_reason.getText().toString(), tv_cancel, tv_ReSchedule);
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog2.dismiss();
            }
        });
        dialog2.show();
    }

    //  ******************* delete Order dialog ********************
    private void ScheduleOrderDialog(final String orderId) {
        final AlertDialog.Builder builder2 = new AlertDialog.Builder(mContext);
        final View view2 = LayoutInflater.from(mContext).inflate(R.layout.popup_schedule_order, null);

        final Date currentTime;

        dateEdittext = view2.findViewById(R.id.dateEdittext);
        tv_time = view2.findViewById(R.id.tv_time);
        final TextView ok = view2.findViewById(R.id.ok);
        final TextView cancel = view2.findViewById(R.id.cancel);

        simpleTimePicker = view2.findViewById(R.id.simpleTimePicker);
        // final TextView tv_time = view2.findViewById(R.id.tv_time);
       /* final CalendarView calender_view = view2.findViewById(R.id.calender_view);
        Calendar calendar = Calendar.getInstance();
        calender_view.setMinDate(calendar.getTimeInMillis());*/

        simpleTimePicker.setIs24HourView(false);
        simpleTimePicker.setCurrentHour(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
        currentTime = Calendar.getInstance().getTime();

        final int h = currentTime.getHours();
        final int m = currentTime.getMinutes();

        Date date = new Date();
        Log.e("date", date + "");
        builder2.setView(view2);
        final AlertDialog dialog2 = builder2.create();
        dialog2.setCancelable(false);
        dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        simpleTimePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker timePicker, int selectedHour, int selectedMinute) {
                if (dateEdittext.getText().toString().equals("")) {
                    Toast.makeText(mContext, "Please select date first", Toast.LENGTH_SHORT).show();
                    time = h + ":" + m;
                    simpleTimePicker.setCurrentHour(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
                } else {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    Date strDate = null;
                    try {
                        strDate = sdf.parse(dateEdittext.getText().toString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                    Date dateobj = new Date();
                    System.out.println(df.format(dateobj));
                    int date1 = dateobj.getDate();
                    int date2 = strDate.getDate();

                    if (date1 == date2) {
                        time = selectedHour + ":" + selectedMinute;
                        if (checktimings(time, "hh:mm")) {
                            time = selectedHour + ":" + selectedMinute;
                            Log.e("Time", time);
                        } else {
                            time = "";
                            Toast.makeText(mContext, "You can not select past time", Toast.LENGTH_SHORT).show();
                            simpleTimePicker.setCurrentHour(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
                            Log.e("Time", time);
                        }
                    } else {
                        time = selectedHour + ":" + selectedMinute;
                        Log.e("Time", time);

                    }
                }

            }
        });

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dateEdittext.getText().toString().isEmpty())
                {
                    Toast.makeText(mContext, "Please Select date ", Toast.LENGTH_SHORT).show();
                }
                else if (tv_time.getText().toString().isEmpty())
                {
                    Toast.makeText(mContext, "Please Select time ", Toast.LENGTH_SHORT).show();

                }
                else {
                    onClickListener.onRescheduleClick(dateEdittext.getText().toString(), time, orderId, dialog2);

                }

            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
            }
        });
        dateEdittext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDobClick();
            }
        });
        tv_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePicker();
            }
        });
        dialog2.show();
    }

    //  ******************* Assigned Order dialog ********************
    private void AssignedOrderDialog() {
        final AlertDialog.Builder builder2 = new AlertDialog.Builder(mContext);
        View view2 = LayoutInflater.from(mContext).inflate(R.layout.popup_assigned_order, null);
        TextView ok = view2.findViewById(R.id.ok);
        builder2.setView(view2);
        final AlertDialog dialog2 = builder2.create();
        dialog2.setCancelable(false);
        dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog2.dismiss();
            }
        });
        dialog2.show();
    }

    public void onDobClick() {
        final Calendar calendar = Calendar.getInstance();
        DatePickerDialog dialog = new DatePickerDialog(mContext, R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker arg0, int year, int month, int day_of_month) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, (month));
                calendar.set(Calendar.DAY_OF_MONTH, day_of_month);
                String myFormat = "yyyy-MM-dd";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
                dateEdittext.setText(sdf.format(calendar.getTime()));
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        dialog.getDatePicker().setMinDate(calendar.getTimeInMillis());// TODO: used to hide previous date,month and year
        calendar.add(Calendar.YEAR, 0);
        calendar.add(Calendar.DAY_OF_MONTH, 60);
        dialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());// TODO: used to hide future date,month and year
        dialog.show();
    }

    //  ********************************** Open Time Picker **************************
    void openTimePicker() {
        TimePickerDialog mTimePicker;
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        mTimePicker = new TimePickerDialog(mContext, android.app.AlertDialog.THEME_HOLO_LIGHT, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                time = selectedHour + ":" + selectedMinute;

                if (dateEdittext.getText().toString().equals("")) {
                    Toast.makeText(mContext, "Please select date first", Toast.LENGTH_SHORT).show();
                    //time = h + ":" + m;
                    // simpleTimePicker.setCurrentHour(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
                } else {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date strDate = null;
                    try {
                        strDate = sdf.parse(dateEdittext.getText().toString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    Date dateobj = new Date();
                    System.out.println(df.format(dateobj));
                    int date1 = dateobj.getDate();
                    int date2 = strDate.getDate();
                    Log.e("date1", date1 + "");
                    Log.e("date2", date2 + "");
                    if (date1 == date2) {
                        time = selectedHour + ":" + selectedMinute;
                        if (checktimings(time, "hh:mm")) {
                            time = selectedHour + ":" + selectedMinute;
                            Log.e("Time", time);
                        } else {
                            time = "";
                            Toast.makeText(mContext, "You can not select past time", Toast.LENGTH_SHORT).show();
                            simpleTimePicker.setCurrentHour(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
                            Log.e("Time", time);

                        }
                        tv_time.setText(time);
                    } else {
                        time = selectedHour + ":" + selectedMinute;
                        Log.e("Time", time);
                        tv_time.setText(time);
                    }

                }
                /*SimpleDateFormat fmt = new SimpleDateFormat("HH:mm");
                Date date = null;
                try {
                    date = fmt.parse(time);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                SimpleDateFormat fmtOut = new SimpleDateFormat("hh:mm");
                String formattedTime = fmtOut.format(date);

                tv_time.setText(formattedTime);
                Log.e("TAG", time);*/
            }
        }, hour, minute, true);//No 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    private boolean checktimings(String time, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
        Date EndTime = null;
        try {
            EndTime = dateFormat.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Date CurrentTime = null;
        try {
            CurrentTime = dateFormat.parse(dateFormat.format(new Date()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (CurrentTime.before(EndTime)) {
            return true;
        } else {
            return false;
        }
    }


}