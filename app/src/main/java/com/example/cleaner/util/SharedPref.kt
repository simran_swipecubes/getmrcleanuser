package com.example.cleaner.util

import android.content.Context
import android.content.SharedPreferences

class SharedPref(val context: Context) {

    private val USER_PREFS = "USER"
    private val USER_ID = "ID"
    private val REFRESH_TOKEN = "token"
    private val USER_NAME = "NMAE"
    private val USER_LAST_NAME = "LAST_NMAE"
    private val USER_EMAIL = "EMAIL"
    private val USER_MOB = "MOB"
    private val USER_PIC = "PIC"
    private val USER_TOTAL_BAL = "BALANCE"
    private val USER_TOTAL_WIN = "WIN"
    private val USER_TOTAL_LOST = "LOST"
    private val USER_TOTAL_PLAYED = "PLAYED"
    private val ADDRESS_ADDRESS = "ADDRESS_ADDRESS"
    private val ADDRESS_ZIPCODE = "ADDRESS_ZIPCODE"
    private val ADDRESS_CITY = "ADDRESS_CITY"
    private val ADDRESS_STATE = "ADDRESS_STATE"
    private val REFER_URL = "REFER_URL"
    private val SOUND = "SOUND"
    private val VIBRATE = "VIBRATE"
    private val TTS = "TTS"


    var sharedPreferences: SharedPreferences = context.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE)
    var editor: SharedPreferences.Editor = sharedPreferences.edit()

    fun getUSER_ID(): String? {
        return sharedPreferences.getString(USER_ID, "")
    }

    fun setUSER_ID(user_id: String) {
        editor.putString(USER_ID, user_id).commit()
    }

    fun getUSER_PIC(): String? {
        return sharedPreferences.getString(USER_PIC, "")
    }

    fun setUSER_PIC(user_pic: String) {
        editor.putString(USER_PIC, user_pic).commit()
    }
    fun getToken(): String? {
        return sharedPreferences.getString(REFRESH_TOKEN, "")
    }

    fun setToken(token: String) {
        editor.putString(REFRESH_TOKEN, token).commit()
    }

    fun getUSER_NAME(): String? {
        return sharedPreferences.getString(USER_NAME, "")
    }

    fun setUSER_NAME(user_name: String) {
        editor.putString(USER_NAME, user_name).commit()
    }

    fun getUSER_LAST_NAME(): String? {
        return sharedPreferences.getString(USER_LAST_NAME, "")
    }

    fun setUSER_LAST_NAME(user_last_name: String) {
        editor.putString(USER_LAST_NAME, user_last_name).commit()
    }

    fun getUSER_EMAIL(): String? {
        return sharedPreferences.getString(USER_EMAIL, "")
    }

    fun setUSER_EMAIL(user_email: String) {
        editor.putString(USER_EMAIL, user_email).commit()
    }

    fun getUSER_MOB(): String? {
        return sharedPreferences.getString(USER_MOB, "")
    }

    fun setUSER_MOB(user_mob: String) {
        editor.putString(USER_MOB, user_mob).commit()
    }

    fun getUSER_TOTAL_BAL(): String? {
        return sharedPreferences.getString(USER_TOTAL_BAL, "")
    }

    fun setUSER_TOTAL_BAL(user_bal: String) {
        editor.putString(USER_TOTAL_BAL, user_bal).commit()
    }

    fun getUSER_TOTAL_WIN(): String? {
        return sharedPreferences.getString(USER_TOTAL_WIN, "")
    }

    fun setUSER_TOTAL_WIN(user_win: String) {
        editor.putString(USER_TOTAL_WIN, user_win).commit()
    }


    fun getUSER_TOTAL_LOST(): String? {
        return sharedPreferences.getString(USER_TOTAL_LOST, "")
    }

    fun setUSER_TOTAL_LOST(user_lost: String) {
        editor.putString(USER_TOTAL_LOST, user_lost).commit()
    }

    fun getUSER_TOTAL_PLAYED(): String? {
        return sharedPreferences.getString(USER_TOTAL_PLAYED, "")
    }

    fun setUSER_TOTAL_PLAYED(user_played: String) {
        editor.putString(USER_TOTAL_PLAYED, user_played).commit()
    }

    fun getADDRESS_ADDRESS(): String? {
        return sharedPreferences.getString(ADDRESS_ADDRESS, "")
    }

    fun setADDRESS_ADDRESS(address_address: String) {
        editor.putString(ADDRESS_ADDRESS, address_address).commit()
    }

    fun getADDRESS_ZIPCODE(): String? {
        return sharedPreferences.getString(ADDRESS_ZIPCODE, "")
    }

    fun setADDRESS_ZIPCODE(address_zipcode: String) {
        editor.putString(ADDRESS_ZIPCODE, address_zipcode).commit()
    }

    fun getADDRESS_CITY(): String? {
        return sharedPreferences.getString(ADDRESS_CITY, "")
    }

    fun setADDRESS_CITY(address_city: String) {
        editor.putString(ADDRESS_CITY, address_city).commit()
    }

    fun getADDRESS_STATE(): String? {
        return sharedPreferences.getString(ADDRESS_STATE, "")
    }

    fun setADDRESS_STATE(address_state: String) {
        editor.putString(ADDRESS_STATE, address_state).commit()
    }

    fun getREFER_URL(): String? {
        return sharedPreferences.getString(REFER_URL, "")
    }

    fun setREFER_URL(refer_url: String) {
        editor.putString(REFER_URL, refer_url).commit()
    }

    fun getSOUND(): String? {
        return sharedPreferences.getString(SOUND, "")
    }

    fun setSOUND(sound: String) {
        editor.putString(SOUND, sound).commit()
    }

    fun getVIBRATE(): String? {
        return sharedPreferences.getString(VIBRATE, "")
    }

    fun setVIBRATE(vibrate: String) {
        editor.putString(VIBRATE, vibrate).commit()
    }

    fun getTTS(): Boolean? {
        return sharedPreferences.getBoolean(TTS, true)
    }

    fun setTTS(tts: Boolean) {
        editor.putBoolean(TTS, tts).commit()
    }

    fun clearSession() {
        editor.clear().commit()
    }
}