package com.example.cleaner.model;

import com.google.gson.annotations.SerializedName;

public class LoginModel {

    /**
     * status : 200
     * data : {"id":"8","phone":"9781943619","email":"love@gmail.com","firstname":"love","last_name":"preet","address":"qwe","city":"ww","state":"we","zip":"142042"}
     * msg : User login successfully.
     */

    @SerializedName("status")
    private int status;
    @SerializedName("data")
    private DataBean data;
    @SerializedName("msg")
    private String msg;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class DataBean {
        /**
         * id : 8
         * phone : 9781943619
         * email : love@gmail.com
         * firstname : love
         * last_name : preet
         * address : qwe
         * city : ww
         * state : we
         * zip : 142042
         */

        @SerializedName("id")
        private String id;
        @SerializedName("phone")
        private String phone;
        @SerializedName("email")
        private String email;
        @SerializedName("firstname")
        private String firstname;
        @SerializedName("last_name")
        private String lastName;
        @SerializedName("address")
        private String address;
        @SerializedName("city")
        private String city;
        @SerializedName("state")
        private String state;
        @SerializedName("zip")
        private String zip;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getZip() {
            return zip;
        }

        public void setZip(String zip) {
            this.zip = zip;
        }
    }
}
