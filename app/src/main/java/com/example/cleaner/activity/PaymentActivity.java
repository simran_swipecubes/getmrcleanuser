package com.example.cleaner.activity;

import android.content.Context;
import android.os.Bundle;

import android.widget.EditText;

import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.example.cleaner.R;
import com.example.cleaner.common.CardForm;
import com.example.cleaner.common.OnPayBtnClickListner;
import com.stripe.android.model.Card;

public class PaymentActivity extends AppCompatActivity {
    CardForm cardForm;
    com.stripe.android.model.Card mycards;
    Context mContext = this;
    private String amountSTR;
    private String FirstName = "";
    private String LastName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        initViews();
    }

    private String getString(EditText ed) {
        return ed.getText().toString().trim();
    }

    private void initViews() {
        cardForm = findViewById(R.id.card_form);
        cardForm.setAmount(amountSTR);
        cardForm.setPayBtnClickListner(new OnPayBtnClickListner() {
            @Override
            public void onClick(Card card) {
                mycards = new Card(card.getNumber(), card.getExpMonth(), card.getExpYear(), card.getCVC(), card.getName(),
                        "", "", "", "", "", "", "usd");
                if (!mycards.validateCard()) {
                    Toast.makeText(mContext, "Please enter valid card details", Toast.LENGTH_SHORT).show();
                } else {
                    String firstWord = mycards.getName().toString();
                    if (firstWord.contains(" ")) {
                        FirstName = firstWord = firstWord.substring(0, firstWord.indexOf(" "));
                        LastName = firstWord = firstWord.substring(1, firstWord.indexOf(" "));
                    } else {
                        FirstName = firstWord;
                    }
                    /* RestServiceConnector.getService().BankTOWallet(
                            SharedPreferencesHelper.getInstance(mContext).getLoginResponse().getData().getId().toString()
                            , amountSTR, FirstName, LastName, mycards.getType().toString(), mycards.getNumber().toString()
                            , mycards.getExpMonth().toString(), mycards.getExpYear().toString(), mycards.getCVC().toString()
                            , callBacks()); */
                }
            }
        });
    }

}
