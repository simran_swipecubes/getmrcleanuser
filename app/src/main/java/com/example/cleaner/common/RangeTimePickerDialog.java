package com.example.cleaner.common;

import android.app.TimePickerDialog;
import android.content.Context;
import android.util.Log;
import android.widget.TimePicker;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.util.Calendar;

public class RangeTimePickerDialog extends TimePickerDialog {

    private int minHour = 0;
    private int minMinute = 0;
    private int minSec = 0;
    private int maxSecond = 60;
    private int currentTime = 0;
    private int maxHour = 24;
    private int maxMinute = 0;
    private int currentSecond = 0;
    private int currentHour = 0;
    private int currentMinute = 0;
    private Calendar calendar = Calendar.getInstance();
    private DateFormat dateFormat;


    public RangeTimePickerDialog(Context context, OnTimeSetListener callBack, int hourOfDay, int minute, int second, boolean is24HourView) {
        super(context, callBack, hourOfDay, minute, is24HourView);

        currentHour = calendar.get(Calendar.HOUR);
        currentMinute = calendar.get(Calendar.MINUTE);
        currentSecond = second;

        dateFormat = DateFormat.getTimeInstance(DateFormat.SHORT);

        try {
            Class<?> superclass = getClass().getSuperclass();
            Field mTimePickerField = superclass.getDeclaredField("mTimePicker");
            mTimePickerField.setAccessible(true);
            TimePicker mTimePicker = (TimePicker) mTimePickerField.get(this);
            mTimePicker.setOnTimeChangedListener(this);
        } catch (NoSuchFieldException e) {
        } catch (IllegalArgumentException e) {
        } catch (IllegalAccessException e) {
        }
    }

    public void setMin(int hour, int minute, int sec) {
        minHour = hour;
        minMinute = minute;
        minSec = sec;
    }

    public void setMax(int hour, int minute, int Second) {
        maxHour = hour;
        maxMinute = minute;
        maxSecond = minute;
    }

    @Override
    public void onTimeChanged(TimePicker view, int picker_hours, int picker_minute) {
        Log.d("TAG", currentHour + "....." + currentMinute);
        boolean validTime = true;
        Calendar calendar = Calendar.getInstance();
        Log.d("TAG", "hh" + String.valueOf(calendar.get(Calendar.HOUR)));

        /*if (picker_hours < minHour || (picker_hours == minHour && picker_minute < minMinute)) {
            Log.d("TAG", "1");
            validTime = false;
        }
        if (picker_hours > maxHour || (picker_hours == maxHour && picker_minute > maxMinute)) {
            Log.d("TAG", "2");
            validTime = false;
        }
*/

        if (validTime) {
            currentHour = picker_hours;
            currentMinute = picker_minute;

        }
        updateTime(currentHour, currentMinute);
        updateDialogTitle(view, currentHour, currentMinute, currentSecond);
    }

    private void updateDialogTitle(TimePicker timePicker, int hourOfDay, int minute, int second) {
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, second);
        String title = dateFormat.format(calendar.getTime());
        setTitle(title);
    }
}


