package com.example.cleaner.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.example.cleaner.R;
import com.example.cleaner.apiservice.CustomCallBacks;
import com.example.cleaner.apiservice.RestServiceConnector;
import com.example.cleaner.model.CommonModel;
import com.example.cleaner.model.LoginModel;
import com.example.cleaner.util.NetworkUtil;
import com.example.cleaner.util.SharedPref;
import com.example.cleaner.util.Util;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener {

    TextView txt_change_pass, title;
    EditText et_new_password, et_old_password, et_con_password;
    String old_pass, new_pass, con_pass;
    Context context;
    SharedPref pref;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass);
        context = ChangePasswordActivity.this;
        pref = new SharedPref(context);
        inItUi();
        //version();
    }

    public void inItUi() {
        txt_change_pass = findViewById(R.id.change_pass_change_pass);
        title = findViewById(R.id.actionbar_title);
        back = findViewById(R.id.action_bar_back);
        et_old_password = findViewById(R.id.change_pass_old_password);
        et_con_password = findViewById(R.id.change_pass_con_password);
        et_new_password = findViewById(R.id.change_pass_new_password);
        txt_change_pass.setOnClickListener(this);
        back.setOnClickListener(this);
        title.setText(R.string.change_password);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.change_pass_change_pass:
                if (NetworkUtil.INSTANCE.isConnected(context)) {
                    validate();
                }
                break;
            case R.id.login_sign_up:
                Intent i = new Intent(context, SignupActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.rightsideanimation, R.anim.leftoutanimation);
                break;
            case R.id.action_bar_back:
                finish();
                break;

        }
    }

    private void validate() {

        old_pass = et_old_password.getText().toString().trim();
        new_pass = et_new_password.getText().toString().trim();
        con_pass = et_con_password.getText().toString().trim();

        if (old_pass.isEmpty()) {
            et_old_password.setError("old password required");
            et_old_password.requestFocus();
            et_old_password.findFocus();
        } else if (new_pass.length() < 5) {
            et_new_password.setError("Poor Password");
            et_new_password.requestFocus();
            et_new_password.findFocus();
        } else if (con_pass.isEmpty()) {
            et_con_password.setError("Password required");
            et_con_password.requestFocus();
            et_con_password.findFocus();
        } else if (!new_pass.equals(con_pass)) {
            et_con_password.setError("Password Not Matched");
            et_con_password.requestFocus();
            et_con_password.findFocus();
        } else {
            RestServiceConnector.getService().changePass(pref.getUSER_ID(), old_pass, con_pass, changePass());
        }
    }

    /*/********************* Calling Register/SignIn Api*****************************/

    public CustomCallBacks<CommonModel> changePass() {
        return new CustomCallBacks<CommonModel>(context, true) {
            @Override
            public void onSucess(CommonModel arg0, Response arg1) {
                if (arg0.getStatus() == 200) {
                    finish();
                    Toast.makeText(context, arg0.getMsg(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, arg0.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(RetrofitError arg0) {
                Toast.makeText(context, "Some thing went wrong.Please Try again", Toast.LENGTH_SHORT).show();
            }
        };
    }

   /*
    public void version() {
        Log.d("TAG", "versn");
        try {

            VersionChecker VersionChecker = new VersionChecker();
            String versionUpdated = VersionChecker.execute().get().toString();
            Log.d("TAG", "version code is  :" + versionUpdated);
            PackageInfo packageInfo = null;
            try {
                packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            int version_code = packageInfo.versionCode;
            String version_name = packageInfo.versionName;
            Log.d("TAG", "updated version code  " + String.valueOf(version_code) + "  " + version_name);
            if (!version_name.equals(versionUpdated)) {
                Log.d("TAG", "u code  " + String.valueOf(version_code) + "  " + version_name);
                String packageName = getApplicationContext().getPackageName();//
                UpdateMeeDialog updateMeeDialog = new UpdateMeeDialog();
                updateMeeDialog.showDialogAddRoute(context, packageName);
                // Toast.makeText(getApplicationContext(), "please updated", Toast.LENGTH_LONG).show();
                Log.d("TAG", "u code  " + String.valueOf(version_code) + "  " + version_name);
            } else {
                // Toast.makeText(getApplicationContext(), " latest updated", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.getStackTrace();
        }
    }*/
}
