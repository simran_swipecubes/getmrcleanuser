package com.example.cleaner.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.example.cleaner.R;
import com.example.cleaner.model.GetAddressModel;

import java.util.List;

public class AddressSpinnerAdapter extends BaseAdapter {
    Context context;
    List<GetAddressModel.Datum> data;
    LayoutInflater inflter;
    onClickListener onClickListener;

    public interface onClickListener {
        void onClicK(String id, int i);

        //  void addressOnZeroIndex(String id);
    }

    public AddressSpinnerAdapter(Context applicationContext, List<GetAddressModel.Datum> data, onClickListener onClickListener) {
        this.context = applicationContext;
        this.data = data;
        this.onClickListener = onClickListener;

        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.items_spinner, null);

        final TextView tv_pieces = view.findViewById(R.id.tv_pieces);
        RelativeLayout rl_spin = view.findViewById(R.id.rl_spin);
        LinearLayout ll_spin_item = view.findViewById(R.id.ll_spin_item);
        tv_pieces.setText(data.get(i).getZip() + "," + data.get(i).getCity());

        // onClickListener.addressOnZeroIndex(data.get(0).getId());
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onClicK(data.get(i).getId(), i);
                tv_pieces.setText(data.get(i).getZip() + "," + data.get(i).getCity());
            }
        });

        return view;
    }


}
