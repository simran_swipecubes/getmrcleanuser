package com.example.cleaner.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import androidx.recyclerview.widget.RecyclerView;
import com.example.cleaner.R;
import com.example.cleaner.activity.SuppotTicketActivity;
import com.example.cleaner.apiservice.CustomCallBacks;
import com.example.cleaner.apiservice.RestServiceConnector;
import com.example.cleaner.model.CommonModel;
import com.example.cleaner.model.HistoryModel;
import com.example.cleaner.util.SharedPref;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {
    private Context mContext;
     SharedPref pref;
    List<HistoryModel.Datum> data;
    AlertDialog dialog2;
    onClickListerner onClickListerner;

    public interface onClickListerner {
        void onClick();
    }

    public HistoryAdapter(Context mContext, List<HistoryModel.Datum> data, onClickListerner onClickListerner) {
        this.mContext = mContext;
        this.data = data;
        this.onClickListerner = onClickListerner;
    }

    @Override
    public HistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_history, parent, false);
        pref = new SharedPref(mContext);
        return new HistoryAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final HistoryAdapter.ViewHolder holder, final int position) {
        holder.tv_title.setText(data.get(position).getTitle());
        holder.tv_price.setText("$ " + data.get(position).getNetAmount());
        holder.tv_date.setText(data.get(position).getBookingDateTime());
        if (data.get(position).getBookingStatus().equals("CO")) {
            holder.tv_status.setText("Completed");
        }
        holder.tv_address.setText(data.get(position).getAddress());
        holder.tv_vendor_name.setText(data.get(position).getVendor());
        holder.rl_rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rateVendorDialog(position);
            }
        });
        if (!data.get(position).getRating().equals("")) {
            holder.rating_bar.setVisibility(View.VISIBLE);
            holder.rating_bar.setRating(Float.valueOf(data.get(position).getRating()));
            holder.rl_rate.setVisibility(View.GONE);
        } else {
            holder.rating_bar.setVisibility(View.GONE);
            holder.rl_rate.setVisibility(View.VISIBLE);
        }
        holder.rl_support_ticket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext, SuppotTicketActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView tv_title, tv_price, tv_date, tv_status, tv_vendor_name, tv_address;
        RelativeLayout rl_rate,rl_support_ticket;
        RatingBar rating_bar;

        public ViewHolder(View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.item_service_image);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_price = itemView.findViewById(R.id.tv_price);
            tv_date = itemView.findViewById(R.id.tv_date);
            tv_status = itemView.findViewById(R.id.tv_status);
            tv_vendor_name = itemView.findViewById(R.id.tv_vendor_name);
            tv_address = itemView.findViewById(R.id.tv_address);
            rl_rate = itemView.findViewById(R.id.rl_rate);
            rl_support_ticket = itemView.findViewById(R.id.rl_support_ticket);
            rating_bar = itemView.findViewById(R.id.rating_bar);
        }
    }

    //  ******************* delete address dialog ********************
    private void rateVendorDialog(final int position) {
        final AlertDialog.Builder builder2 = new AlertDialog.Builder(mContext);
        View view2 = LayoutInflater.from(mContext).inflate(R.layout.popup_rating, null);
        TextView cancel = view2.findViewById(R.id.cancel);
        TextView ok = view2.findViewById(R.id.ok);
        final RatingBar rating = view2.findViewById(R.id.rating);
        builder2.setView(view2);
        dialog2 = builder2.create();
        dialog2.setCancelable(false);
        dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RestServiceConnector.getService().rating(data.get(position).getStaffIds(),
                        data.get(position).getOrderId(),
                        rating.getRating(),
                        giveVendorRating());
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog2.dismiss();
            }
        });
        dialog2.show();
    }

    private CustomCallBacks<CommonModel> giveVendorRating() {
        return new CustomCallBacks<CommonModel>(mContext, true) {
            @Override
            public void onSucess(CommonModel arg0, Response arg1) {
                if (arg0.getStatus() == 200) {
                    dialog2.dismiss();
                    Toast.makeText(mContext, arg0.getMsg(), Toast.LENGTH_SHORT).show();
                    onClickListerner.onClick();
                } else {
                    dialog2.dismiss();
                    Toast.makeText(mContext, arg0.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(RetrofitError arg0) {
                dialog2.dismiss();
                Toast.makeText(mContext, arg0.getMessage(), Toast.LENGTH_SHORT).show();
            }
        };
    }

}
