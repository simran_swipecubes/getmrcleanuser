package com.example.cleaner.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.example.cleaner.R;
import com.example.cleaner.activity.*;
import com.example.cleaner.util.SharedPref;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.example.cleaner.activity.MainActivity.drawerLayout;

public class NavFragment extends Fragment implements View.OnClickListener {
    private LinearLayout ll_my_profile, ll_change_pass, ll_logout,
            ll_my_past_booking, ll_My_future_booking, my_address, nav_ll_support_ticket;
    Context context;
    SharedPref pref;
    public static TextView nav_name, nav_email;
    public static CircleImageView nav_profile;

    public NavFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        context = getActivity();
        pref = new SharedPref(context);
        return inflater.inflate(R.layout.nav_bar, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        nav_name = view.findViewById(R.id.nav_name);
        nav_email = view.findViewById(R.id.nav_email);
        nav_profile = view.findViewById(R.id.nav_profile);
        nav_ll_support_ticket = view.findViewById(R.id.nav_ll_support_ticket);
        ll_my_profile = view.findViewById(R.id.nav_ll_my_profile);
        ll_change_pass = view.findViewById(R.id.nav_ll_change_password);
        ll_logout = view.findViewById(R.id.nav_ll_logout);
        ll_my_past_booking = view.findViewById(R.id.nav_ll_my_past_booking);
        ll_My_future_booking = view.findViewById(R.id.nav_ll_my_future_booking);
        my_address = view.findViewById(R.id.nav_ll_my_address);
        ll_my_profile.setOnClickListener(this);
        ll_my_past_booking.setOnClickListener(this);
        nav_ll_support_ticket.setOnClickListener(this);
        ll_My_future_booking.setOnClickListener(this);
        my_address.setOnClickListener(this);
        ll_change_pass.setOnClickListener(this);
        ll_logout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.nav_ll_my_profile:
                drawerLayout.closeDrawers();
                startActivity(new Intent(context, UserProfileActivity.class));
                break;
            case R.id.nav_ll_change_password:
                drawerLayout.closeDrawers();
                startActivity(new Intent(context, ChangePasswordActivity.class));
                break;
            case R.id.nav_ll_my_address:
                drawerLayout.closeDrawers();
                startActivity(new Intent(context, MyAddressActivity.class));
                break;
            case R.id.nav_ll_my_past_booking:
                drawerLayout.closeDrawers();
                startActivity(new Intent(context, HistoryActivity.class));
                break;
            case R.id.nav_ll_my_future_booking:
                drawerLayout.closeDrawers();
                startActivity(new Intent(context, ScheduleActivity.class).putExtra("title", getResources().getString(R.string.future_bookings)));
                break;
            case R.id.nav_ll_support_ticket:
                drawerLayout.closeDrawers();
                startActivity(new Intent(context, SuppotTicketActivity.class));
                break;
            case R.id.nav_ll_logout:
                drawerLayout.closeDrawers();
                logout_Dialog();
                break;
        }
    }

    //  ******************* delete address dialog ********************
    private void logout_Dialog() {
        final AlertDialog.Builder builder2 = new AlertDialog.Builder(getActivity());
        View view2 = LayoutInflater.from(getActivity()).inflate(R.layout.popup_logout, null);
        TextView cancel = view2.findViewById(R.id.cancel);
        TextView ok = view2.findViewById(R.id.ok);
        builder2.setView(view2);
        final AlertDialog dialog2 = builder2.create();
        dialog2.setCancelable(false);
        dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog2.dismiss();
                pref.clearSession();
                startActivity(new Intent(context, LoginActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                getActivity().finish();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog2.dismiss();
            }
        });
        dialog2.show();
    }

}



