package com.example.cleaner.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.example.cleaner.R;
import com.example.cleaner.activity.AddAddressActivity;
import com.example.cleaner.model.GetAddressModel;

import java.util.List;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder> {
    private Activity activity;
    private List<GetAddressModel.Datum> data;
    private onClickListener onClickListener;

    public interface onClickListener {
        void onClick(String id, int position);
    }

    public AddressAdapter(Activity activity, List<GetAddressModel.Datum> data, onClickListener onClickListener) {
        this.activity = activity;
        this.data = data;
        this.onClickListener = onClickListener;
    }

    @Override
    public AddressAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(activity).inflate(R.layout.item_addresses, parent, false);
        return new AddressAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final AddressAdapter.ViewHolder holder, final int position) {
        holder.tv_name.setText(data.get(position).getFirstName());
        holder.tv_address.setText(data.get(position).getAddress());
        holder.tv_pin.setText(data.get(position).getCity() + " " + data.get(position).getZip() + " " + data.get(position).getState());
        if (data.get(position).getDefaultaddr().equals("1")) {
            holder.user_profile_edit_img.setVisibility(View.GONE);
        } else {
            holder.user_profile_edit_img.setVisibility(View.VISIBLE);
        }
        holder.user_profile_edit_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteProductDialog(position);
            }
        });
        holder.iv_address_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.startActivity(new Intent(activity, AddAddressActivity.class).putExtra("where", "edit").
                        putExtra("address_id", data.get(position).getId()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name, tv_address, tv_pin;
        private ImageView user_profile_edit_img, iv_address_edit;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_address = itemView.findViewById(R.id.tv_address);
            tv_pin = itemView.findViewById(R.id.tv_pin);
            user_profile_edit_img = itemView.findViewById(R.id.user_profile_edit_img);
            iv_address_edit = itemView.findViewById(R.id.iv_address_edit);
        }
    }

    //  ******************* delete address dialog ********************
    private void deleteProductDialog(final int pos) {
        final AlertDialog.Builder builder2 = new AlertDialog.Builder(activity);
        View view2 = LayoutInflater.from(activity).inflate(R.layout.popup_delete, null);
        TextView cancel = view2.findViewById(R.id.cancel);
        TextView ok = view2.findViewById(R.id.ok);
        builder2.setView(view2);
        final AlertDialog dialog2 = builder2.create();
        dialog2.setCancelable(false);
        dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog2.dismiss();
                onClickListener.onClick(data.get(pos).getId(), pos);
                data.remove(pos);

                notifyDataSetChanged();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog2.dismiss();
            }
        });
        dialog2.show();
    }

}
