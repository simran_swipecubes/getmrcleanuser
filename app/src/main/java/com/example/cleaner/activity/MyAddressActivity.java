package com.example.cleaner.activity;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.cleaner.R;
import com.example.cleaner.adapter.AddressAdapter;
import com.example.cleaner.apiservice.CustomCallBacks;
import com.example.cleaner.apiservice.RestServiceConnector;
import com.example.cleaner.model.CommonModel;
import com.example.cleaner.model.GetAddressModel;
import com.example.cleaner.util.SharedPref;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.HashMap;
import java.util.Map;

public class MyAddressActivity extends AppCompatActivity implements View.OnClickListener {

    private Activity activity = this;
    private RecyclerView rv_address;
    private ImageView user_profile_back;
    private TextView tv_add_address, tv_noAddress;
    SharedPref pref;
    AddressAdapter addressAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_address);

        initUi();
    }

    private void initUi() {
        pref = new SharedPref(this);
        rv_address = findViewById(R.id.rv_address);
        user_profile_back = findViewById(R.id.user_profile_back);
        tv_add_address = findViewById(R.id.tv_add_address);
        tv_noAddress = findViewById(R.id.tv_noAddress);

        user_profile_back.setOnClickListener(this);
        tv_add_address.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.user_profile_back:
                onBackPressed();
                break;
            case R.id.tv_add_address:
                startActivity(new Intent(this, AddAddressActivity.class).putExtra("where","my_address"));
                break;
        }
    }

    // **************** get Address List APi is here **************
    private CustomCallBacks<GetAddressModel> GetAddressListcallBacks() {
        return new CustomCallBacks<GetAddressModel>(activity, true) {
            @Override
            public void onSucess(GetAddressModel arg0, Response arg1) {
                if (arg0.getStatus() == 200) {
                    if (arg0.getData().size() != 0)
                    {
                        rv_address.setLayoutManager(new LinearLayoutManager(activity));
                        tv_noAddress.setVisibility(View.GONE);

                        addressAdapter = new AddressAdapter(activity, arg0.getData(), new AddressAdapter.onClickListener() {
                            @Override
                            public void onClick(String id, int position) {
                                Map<String, String> data = new HashMap<>();
                                data.put("aid", id);
                                RestServiceConnector.getService().deleteAddress(data, deleteAddressModel(position));
                            }
                        });

                        rv_address.setAdapter(addressAdapter);
                    }
                    else {
                        tv_noAddress.setVisibility(View.VISIBLE);

                    }

                } else {
                    tv_noAddress.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void onFailure(RetrofitError arg0) {
                Toast.makeText(activity, arg0.getMessage(), Toast.LENGTH_SHORT).show();

            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        Map<String, String> data = new HashMap<String, String>();
        data.put("uid", pref.getUSER_ID());

        RestServiceConnector.getService().getAddressList(data, GetAddressListcallBacks());
    }


    // ************ delete Address APi is here ***********
    private CustomCallBacks<CommonModel> deleteAddressModel(final int position) {
        return new CustomCallBacks<CommonModel>(activity, true) {
            @Override
            public void onSucess(CommonModel arg0, Response arg1) {
                if (arg0.getStatus() == 200) {
                   /* addressAdapter.notifyItemRemoved(position);
                    addressAdapter.notifyDataSetChanged();*/
                    Toast.makeText(activity, arg0.getMsg(), Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(activity, arg0.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(RetrofitError arg0) {
                Toast.makeText(activity, arg0.getMessage(), Toast.LENGTH_SHORT).show();

            }
        };
    }
}
