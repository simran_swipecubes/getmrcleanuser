package com.example.cleaner.fcm;

import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import androidx.core.app.NotificationCompat;
import com.example.cleaner.R;
import com.example.cleaner.activity.LoginActivity;
import com.example.cleaner.activity.MainActivity;
import com.example.cleaner.activity.ScheduleActivity;
import com.example.cleaner.util.SharedPref;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class MyFirebaseMessaging extends FirebaseMessagingService {
    public
    Context context;
    public static String new_order_count;
    Notification mNotification;
    SharedPref pref;
    Activity activity;
    Intent intent;
    String type;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);

        Log.e("NEW_TOKEN", s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Map<String, String> datamap = remoteMessage.getData();
        type = datamap.get("type");
        //new_order_count = datamap.get("notification_count");
        // Log.e("TAG", new_order_count);
        pref = new SharedPref(this);
        Log.e("TYpe", pref.getUSER_ID());
        sendNotification(datamap);
    }

    private void sendNotification(Map<String, String> map) {
       /* if (type.equals("book")) {
            intent = new Intent(this, ScheduleActivity.class);
        } else if (type.equals("complete")) {
            intent = new Intent(this, HistoryActivity.class);
            //intent = new Intent(this, HistoryActivity.class);
        } else if (type.equals("assign")) {
            intent = new Intent(this, ScheduleActivity.class);
        } else {*/
    //    intent = new Intent(this, MainActivity.class);
        // }
        if(map.get("type").equals("book")){
            intent = new Intent(this, ScheduleActivity.class);
            intent.putExtra("ride", "0");
            int notificationId = 1;
            String channelId = "channel-01";
            String channelName = "Channel Name";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            //Uri alarmSound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.sirena_ambulanza);
            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                NotificationChannel mChannel = new NotificationChannel(channelId, channelName, importance);
                mNotificationManager.createNotificationChannel(mChannel);
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.app_icon)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.app_icon))
                        .setContentTitle(map.get("title"))
                        .setContentText(map.get("body"));
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                AudioAttributes attributes = new AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                        .build();
                mChannel.setSound(alarmSound, attributes);
                stackBuilder.addNextIntent(intent);
                PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
                mBuilder.setContentIntent(resultPendingIntent);
                mNotificationManager.notify(notificationId, mBuilder.build());

            } else {
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                Context context = getBaseContext();
                NotificationCompat.Builder mBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(context).
                        setContentTitle(map.get("title"))
                        .setContentText(map.get("message")).
                                setSmallIcon(R.mipmap.app_icon).
                                setAutoCancel(false).
                                setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.app_icon)).
                                setStyle(new NotificationCompat.BigTextStyle().bigText(map.get("message"))).
                                setPriority(Notification.PRIORITY_HIGH);
                mBuilder.setSound(alarmSound);
                mBuilder.setAutoCancel(true);
                mBuilder.setContentIntent(pendingIntent);
                NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.notify(0, mBuilder.build());
            }
        }else {
            if (pref.getUSER_ID() == null) {
                intent = new Intent(this, LoginActivity.class);
                intent.putExtra("ride", "0");
                int notificationId = 1;
                String channelId = "channel-01";
                String channelName = "Channel Name";
                int importance = NotificationManager.IMPORTANCE_HIGH;
                //Uri alarmSound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.sirena_ambulanza);
                Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    NotificationChannel mChannel = new NotificationChannel(channelId, channelName, importance);
                    mNotificationManager.createNotificationChannel(mChannel);
                    NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, channelId)
                            .setSmallIcon(R.mipmap.app_icon)
                            .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.app_icon))
                            .setContentTitle(map.get("title"))
                            .setContentText(map.get("message"));
                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                    AudioAttributes attributes = new AudioAttributes.Builder()
                            .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                            .build();
                    mChannel.setSound(alarmSound, attributes);
                    stackBuilder.addNextIntent(intent);
                    PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                            0,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );
                    mBuilder.setContentIntent(resultPendingIntent);
                    mNotificationManager.notify(notificationId, mBuilder.build());

                } else {
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    Context context = getBaseContext();
                    NotificationCompat.Builder mBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(context).
                            setContentTitle(map.get("title"))
                            .setContentText(map.get("message")).
                                    setSmallIcon(R.mipmap.app_icon).
                                    setAutoCancel(false).
                                    setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.app_icon)).
                                    setStyle(new NotificationCompat.BigTextStyle().bigText(map.get("message"))).
                                    setPriority(Notification.PRIORITY_HIGH);
                    mBuilder.setSound(alarmSound);
                    mBuilder.setAutoCancel(true);
                    mBuilder.setContentIntent(pendingIntent);
                    NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    mNotificationManager.notify(0, mBuilder.build());
                }
            } else {
                intent = new Intent(this, MainActivity.class);
                intent.putExtra("ride", "0");
                int notificationId = 1;
                String channelId = "channel-01";
                String channelName = "Channel Name";
                int importance = NotificationManager.IMPORTANCE_HIGH;
                //Uri alarmSound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.sirena_ambulanza);
                Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    NotificationChannel mChannel = new NotificationChannel(channelId, channelName, importance);
                    mNotificationManager.createNotificationChannel(mChannel);
                    NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, channelId)
                            .setSmallIcon(R.mipmap.app_icon)
                            .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.app_icon))
                            .setContentTitle(map.get("title"))
                            .setContentText(map.get("message"));
                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                    AudioAttributes attributes = new AudioAttributes.Builder()
                            .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                            .build();
                    mChannel.setSound(alarmSound, attributes);
                    stackBuilder.addNextIntent(intent);
                    PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                            0,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );
                    mBuilder.setContentIntent(resultPendingIntent);
                    mNotificationManager.notify(notificationId, mBuilder.build());

                } else {
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    Context context = getBaseContext();
                    NotificationCompat.Builder mBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(context).
                            setContentTitle(map.get("title"))
                            .setContentText(map.get("message")).
                                    setSmallIcon(R.mipmap.app_icon).
                                    setAutoCancel(false).
                                    setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.app_icon)).
                                    setStyle(new NotificationCompat.BigTextStyle().bigText(map.get("message"))).
                                    setPriority(Notification.PRIORITY_HIGH);
                    mBuilder.setSound(alarmSound);
                    mBuilder.setAutoCancel(true);
                    mBuilder.setContentIntent(pendingIntent);
                    NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    mNotificationManager.notify(0, mBuilder.build());
                }
            }
        }

    }
}



/*    private void sendNotification(Map<String, String> map) {
        Uri alarmSound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.sirena_ambulanza);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Intent intent = new Intent(this, NewOrderActivity.class);
            int notificationId = 1;
            String channelId = "channel-01";
            String channelName = "Channel Name";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel mChannel = new NotificationChannel(channelId, channelName, importance);
            mNotificationManager.createNotificationChannel(mChannel);
            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, channelId)
                    .setSmallIcon(R.mipmap.appicon)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.gogtariocn))
                    .setContentTitle(map.get("title"))
                    .setSound(alarmSound)
                    .setContentText(map.get("body"));
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
            stackBuilder.addNextIntent(intent);
            mChannel.setSound(alarmSound,attributes);
            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                    0,
                    PendingIntent.FLAG_UPDATE_CURRENT
            );
            mBuilder.setContentIntent(resultPendingIntent);
          //  mBuilder.setSound(alarmSound);
            mNotification = mBuilder.build();
            mBuilder.setAutoCancel(true);
            mNotification.flags = Notification.DEFAULT_LIGHTS | Notification.FLAG_AUTO_CANCEL | Notification.DEFAULT_VIBRATE;
            mNotificationManager.notify(notificationId, mNotification);
        } else {
            Intent intent = new Intent(this, NewOrderActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            Context context = getBaseContext();
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context).
                    setContentTitle(map.get("title"))
                    .setContentText(map.get("body")).
                            setSmallIcon(R.mipmap.appicon).
                            setAutoCancel(false).
                            setSound(alarmSound).
                            setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.gogtariocn)).
                            setStyle(new NotificationCompat.BigTextStyle().bigText(map.get("message"))).
                            setPriority(NotificationCompat.PRIORITY_HIGH);
            mBuilder.setSound(alarmSound);
            mBuilder.setAutoCancel(true);
            mBuilder.setContentIntent(pendingIntent);
            mNotification = mBuilder.build();
            NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            mNotification.flags = Notification.DEFAULT_LIGHTS | Notification.FLAG_AUTO_CANCEL | Notification.DEFAULT_VIBRATE;
            mNotificationManager.notify(0, mNotification);
        }
    }*/



