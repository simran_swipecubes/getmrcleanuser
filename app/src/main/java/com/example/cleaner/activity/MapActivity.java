package com.example.cleaner.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import com.example.cleaner.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, LocationListener, GoogleMap.OnCameraChangeListener, View.OnClickListener {

    TextView txt_location;
    LinearLayout ll_map;
    Button btn_save;
    String city, postalCode, state;
    String lat, lng;
    SupportMapFragment mapFragment;
    GoogleMap map;
    LatLng startLatLng;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    Double lat_changeaddress1, lng_changeaddress1;
    String destination_name, pincode;
    LatLng changelatlng1;
    Intent intent;
    private static final LatLngBounds BOUNDS_INDIA = new LatLngBounds(new LatLng(23.63936, 68.14712),
            new LatLng(28.20453, 97.34466));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        initUis();
        mapFragment = (SupportMapFragment) this.getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void initUis() {
        intent = getIntent();
        txt_location = findViewById(R.id.txt_location);
        ll_map = findViewById(R.id.ll_map);
        btn_save = findViewById(R.id.btn_save);
        btn_save.setOnClickListener(this);
        txt_location.setOnClickListener(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        startLatLng = new LatLng(SplashCopyActivity.lat, SplashCopyActivity.lng);
        map.moveCamera(CameraUpdateFactory.newLatLng(startLatLng));
        map.animateCamera(CameraUpdateFactory.zoomTo(15));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);
        if (map != null) {
            if (ActivityCompat.checkSelfPermission(this.getApplicationContext()
                    , Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this.getApplicationContext()
                    , Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
        }

        map.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                LatLng midLatLng = map.getCameraPosition().target;
                lat = String.valueOf(midLatLng.latitude);
                lng = String.valueOf(midLatLng.longitude);

              /*  prefStore.saveString("addresslat", String.valueOf(lat));
                  prefStore.saveString("addresslng", String.valueOf(lng));*/

                Geocoder geocoder;
                List<Address> addresses = null;
                geocoder = new Geocoder(MapActivity.this, Locale.getDefault());

                try {
                    addresses = geocoder.getFromLocation(midLatLng.latitude, midLatLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    city = addresses.get(0).getLocality();
                    // String state = addresses.get(0).getAdminArea();
                    // String country = addresses.get(0).getCountryName();
                    pincode = addresses.get(0).getPostalCode();
                    state = addresses.get(0).getAdminArea();
                    String knownName = addresses.get(0).getFeatureName();

                    txt_location.setText(address);
                } catch (Exception e) {

                }
            }
        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
      /*  map.moveCamera(CameraUpdateFactory.newLatLng(SplashActivity.changelatlng));
        map.animateCamera(CameraUpdateFactory.zoomTo(12));*/
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onCameraChange(CameraPosition position) {
        map.clear();
        map.addMarker(new MarkerOptions()
                .position(position.target)
                .title(position.toString())
        );
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(MapActivity.this, data);
                Log.i("Place: ", place.getName().toString() + " " + place.getAddress());
                destination_name = place.getAddress() + "";
                String lati = String.valueOf(place.getLatLng());
                LatLng latlng2 = place.getLatLng();
                Double lat1 = latlng2.latitude;
                Double lng2 = latlng2.longitude;
                String cityChange = String.valueOf(place.getName());
                String[] latlng = lati.split("lat/lng:");
                String latlng1 = latlng[1];
                String[] latlngArray = latlng1.split(",");
                String lat12 = latlngArray[0];
                String lat13 = latlngArray[1];
                lat = (lat12.replaceAll("\\(", ""));
                lng = (lat13.replaceAll("\\)", ""));
                lat_changeaddress1 = Double.valueOf(lat);
                lng_changeaddress1 = Double.valueOf(lng);

                //city = place.getName() + "";
                //  Log.e("TAG", place.getName() + "");

                changelatlng1 = new LatLng(lat_changeaddress1, lng_changeaddress1);
                Geocoder geocoder = new Geocoder(this, Locale.getDefault());
                try {
                    map.moveCamera(CameraUpdateFactory.newLatLng(changelatlng1));

                } catch (Exception e) {

                }

                map.animateCamera(CameraUpdateFactory.zoomTo(15));
                //     map.addMarker(new MarkerOptions().position(changelatlng1));
                try {
                    List<Address> addresses = geocoder.getFromLocation(lat1, lng2, 1);
                    pincode = addresses.get(0).getPostalCode();
                    state = addresses.get(0).getAdminArea();
                    city = addresses.get(0).getLocality();

                    Log.e("TAG", state);
                    Log.e("TAG", pincode);
                    Log.e("TAG", city);
                    //Log.e("TAG", city);
                } catch (Exception e) {

                }
                txt_location.setText(destination_name);
            }

        } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
            Status status = PlaceAutocomplete.getStatus(MapActivity.this, data);                // TODO: Handle the error.                Log.i("place status", status.getStatusMessage());            } else if (resultCode == RESULT_CANCELED) {                // The user canceled the operation.            }        }    }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_save:
                if (txt_location.getText().toString().isEmpty()) {
                    Toast.makeText(this, "Select your location first", Toast.LENGTH_SHORT).show();
                } else {

                    if (intent.getStringExtra("type").equals("address")) {
                        Intent intent = new Intent();
                        intent.putExtra("txt_location", txt_location.getText().toString());
                        intent.putExtra("lat", lat);
                        intent.putExtra("lng", lng);
                        intent.putExtra("pincode", pincode);
                        intent.putExtra("state", state);
                        intent.putExtra("city", city);
                        intent.putExtra("mapActivity", "1");
                        setResult(12, intent);
                        finish();
                    }
                    if (intent.getStringExtra("type").equals("sign_up")) {
                        Intent intent = new Intent();
                        intent.putExtra("txt_location", txt_location.getText().toString());
                        intent.putExtra("lat", lat);
                        intent.putExtra("lng", lng);
                        intent.putExtra("pincode", pincode);
                        intent.putExtra("state", state);
                        intent.putExtra("city", city);
                        intent.putExtra("mapActivity", "1");
                        setResult(11, intent);
                        finish();
                    }

                }
                break;
            case R.id.txt_location:
                Intent intent = null;
                try {
                    try {
                        intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).setBoundsBias(BOUNDS_INDIA).
                                build(MapActivity.this);
                    } catch (GooglePlayServicesRepairableException e) {
                        e.printStackTrace();
                    }
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                break;
        }
    }
}




