package com.example.cleaner.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.applandeo.materialcalendarview.EventDay;
import com.example.cleaner.R;
import com.example.cleaner.adapter.PastBookingAdapter;
import com.example.cleaner.apiservice.CustomCallBacks;
import com.example.cleaner.apiservice.RestServiceConnector;
import com.example.cleaner.model.BookingModel;
import com.example.cleaner.model.CommonModel;
import com.example.cleaner.util.CalenderView.data.Day;
import com.example.cleaner.util.CalenderView.widget.CollapsibleCalendar;
import com.example.cleaner.util.SharedPref;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.*;

public class ScheduleActivity extends AppCompatActivity implements View.OnClickListener {
    Context context;
    RecyclerView rv;
    ArrayList<String> service_list;
    LinearLayoutManager lm;
    SharedPref pref;
    ImageView back;
    TextView title, tv_no_history;
    CollapsibleCalendar collapsibleCalendar;
    BookingModel bookingModel;
    String datesss = "";

    public static String checkDigit(int number) {
        return number <= 9 ? "0" + number : String.valueOf(number);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);
        context = ScheduleActivity.this;
        initUis();
    }

    private void initUis() {
        pref = new SharedPref(context);
        title = findViewById(R.id.actionbar_title);
        back = findViewById(R.id.action_bar_back);
        title.setText("Schedule");
        rv = findViewById(R.id.rv);
        tv_no_history = findViewById(R.id.tv_no_history);
        service_list = new ArrayList<>();
        lm = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        rv.setLayoutManager(lm);
        collapsibleCalendar = findViewById(R.id.calendarView);

        back.setOnClickListener(this);
        Map<String, String> data = new HashMap<>();
        data.put("client_id", pref.getUSER_ID());
        RestServiceConnector.getService().BookingsList(data, ServicesListCallback());
        Calendar calendar = Calendar.getInstance();
        Log.e("calendar", calendar + "");
        List<EventDay> events = new ArrayList<>();
        events.add(new EventDay(calendar, R.drawable.select_shape));

        collapsibleCalendar.setCalendarListener(new CollapsibleCalendar.CalendarListener() {
            @Override
            public void onDaySelect() {
                Day day = collapsibleCalendar.getSelectedDay();
                String date = day.getYear() + "-" + checkDigit((day.getMonth() + 1)) + "-" + checkDigit(day.getDay());
                Log.e("Date", date);
                boolean isEvent = false;
                List<HashMap<String, String>> listdata = new ArrayList<>();
                if (bookingModel != null) {
                    if (bookingModel.getData().size() > 0) {
                        for (int i = 0; i < bookingModel.getData().size(); i++) {
                            String book_date = bookingModel.getData().get(i).getBookingDateTime();
                            String[] parts = book_date.split(" ");
                            String part1 = parts[0]; // 004
                            String part2 = parts[1]; // 034556
                            Log.e("part1", part1);
                            Log.e("part2", part2);
                            if (date.equalsIgnoreCase(part1)) {
                                String dat = bookingModel.getData().get(i).getBookingDateTime();
                                String image = bookingModel.getData().get(i).getImage();
                                String title = bookingModel.getData().get(i).getTitle();
                                String price = bookingModel.getData().get(i).getNetAmount();
                                String status = bookingModel.getData().get(i).getBookingStatus();
                                String vendor_name = bookingModel.getData().get(i).getVendor();
                                String address = bookingModel.getData().get(i).getAddress();
                                String o_id = bookingModel.getData().get(i).getOrderId();
                                String staff_id = bookingModel.getData().get(i).getStaffIds();
                                String booking_status = bookingModel.getData().get(i).getBookingStatus();

                                HashMap<String, String> map = new HashMap<>();
                                map.put("image", image);
                                map.put("date", dat);
                                map.put("title", title);
                                map.put("price", price);
                                map.put("status", status);
                                map.put("vendor_name", vendor_name);
                                map.put("address", address);
                                map.put("o_id", o_id);
                                map.put("staff_id", staff_id);
                                map.put("booking_status", booking_status);
                                listdata.add(map);
                                isEvent = true;
                            }
                        }
                    }
                } else {
                    tv_no_history.setVisibility(View.VISIBLE);
                    //Toast.makeText(context, "No bookings Available", Toast.LENGTH_SHORT).show();
                }
                if (isEvent == true) {
                    rv.setVisibility(View.VISIBLE);
                    tv_no_history.setVisibility(View.GONE);
                    rv.setAdapter(new PastBookingAdapter(context, listdata, datesss, new PastBookingAdapter.onClickListener() {
                        @Override
                        public void onClick(String orderId, String s, TextView tv_cancel, TextView tv_ReSchedule) {
                            RestServiceConnector.getService().cancelBooking(orderId, s, cancelBooingCallback(tv_cancel, tv_ReSchedule));
                        }

                        @Override
                        public void onRescheduleClick(String formattedDate, String time, String orderId, AlertDialog dialog2) {
                            RestServiceConnector.getService().reschedule(orderId, formattedDate, time, rescheduleCallbacks(dialog2));
                        }
                    }));
                } else {
                    rv.setVisibility(View.GONE);
                    tv_no_history.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onItemClick(View view) {
            }

            @Override
            public void onDataUpdate() {
            }

            @Override
            public void onMonthChange() {
            }

            @Override
            public void onWeekChange(int i) {
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_sign_up:
                Intent i = new Intent(context, SignupActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.rightsideanimation, R.anim.leftoutanimation);
                break;
            case R.id.action_bar_back:
                finish();
                break;
        }
    }

    public CustomCallBacks<CommonModel> getlist() {
        return new CustomCallBacks<CommonModel>(context, true) {
            @Override
            public void onSucess(CommonModel arg0, Response arg1) {
                if (arg0.getStatus() == 200) {
                    finish();
                    Toast.makeText(context, arg0.getMsg(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, arg0.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(RetrofitError arg0) {
                Toast.makeText(context, "Something went wrong.Please Try again", Toast.LENGTH_SHORT).show();
            }
        };
    }

    // *********** Cancel booking  APi is here **************
    private CustomCallBacks<CommonModel> cancelBooingCallback(final TextView tv_cancel, final TextView tv_ReSchedule) {
        return new CustomCallBacks<CommonModel>(context, true) {
            @Override
            public void onSucess(CommonModel arg0, Response arg1) {
                if (arg0.getStatus() == 200) {
                    tv_cancel.setText("Cancelled");
                    tv_cancel.setClickable(false);
                    tv_ReSchedule.setVisibility(View.GONE);
                    Toast.makeText(ScheduleActivity.this, arg0.getMsg(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ScheduleActivity.this, arg0.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(RetrofitError arg0) {
                Toast.makeText(ScheduleActivity.this, arg0.getMessage(), Toast.LENGTH_SHORT).show();
            }
        };
    }

    // ************ Reschedule booking APi is here ***********
    private CustomCallBacks<CommonModel> rescheduleCallbacks(final AlertDialog dialog2) {
        return new CustomCallBacks<CommonModel>(context, true) {
            @Override
            public void onSucess(CommonModel arg0, Response arg1) {
                if (arg0.getStatus() == 200) {
                    dialog2.dismiss();
                    Map<String, String> data = new HashMap<>();
                    data.put("client_id", pref.getUSER_ID());
                    RestServiceConnector.getService().BookingsList(data, ServicesListCallback());
                    Calendar calendar = Calendar.getInstance();
                    Log.e("calendar", calendar + "");
                    List<EventDay> events = new ArrayList<>();
                    Toast.makeText(ScheduleActivity.this, arg0.getMsg(), Toast.LENGTH_SHORT).show();
                } else {
                    dialog2.dismiss();
                    Toast.makeText(ScheduleActivity.this, arg0.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(RetrofitError arg0) {
                dialog2.dismiss();
                Toast.makeText(ScheduleActivity.this, arg0.getMessage(), Toast.LENGTH_SHORT).show();
            }
        };
    }

    public CustomCallBacks<BookingModel> ServicesListCallback() {
        return new CustomCallBacks<BookingModel>(context, true) {
            @Override
            public void onSucess(BookingModel arg0, Response arg1) {
                if (arg0.getStatus() == 200) {
                    bookingModel = arg0;
                    List<HashMap<String, String>> list = new ArrayList<>();
                    if (bookingModel.getData().size() > 0) {

                        for (int i = 0; i < bookingModel.getData().size(); i++) {
                            String date_time = bookingModel.getData().get(i).getBookingDateTime();
                            String[] parts = date_time.split(" ");
                            String part1 = parts[0]; // 004
                            String part2 = parts[1]; // 034556
                            Log.e("part1", part1);
                            Log.e("part2", part2);
                            String year = bookingModel.getData().get(i).getBookingDateTime().substring(0, 4);
                            String month = bookingModel.getData().get(i).getBookingDateTime().substring(5, 7);
                            String date = bookingModel.getData().get(i).getBookingDateTime().substring(8, 10);

                            Day day = new Day(Integer.parseInt(year), Integer.parseInt(month), Integer.parseInt(date));
                            collapsibleCalendar.setSelectedItem(day);
                            collapsibleCalendar.addEventTag(Integer.parseInt(year), Integer.parseInt(month), Integer.parseInt(date), R.color.red);

                            String dat = bookingModel.getData().get(i).getBookingDateTime();
                            String image = bookingModel.getData().get(i).getImage();
                            String title = bookingModel.getData().get(i).getTitle();
                            String price = bookingModel.getData().get(i).getNetAmount();
                            String status = bookingModel.getData().get(i).getBookingStatus();
                            String vendor_name = bookingModel.getData().get(i).getVendor();
                            String address = bookingModel.getData().get(i).getAddress();
                            String o_id = bookingModel.getData().get(i).getOrderId();
                            String staff_id = bookingModel.getData().get(i).getStaffIds();
                            String booking_status = bookingModel.getData().get(i).getBookingStatus();

                            HashMap<String, String> map = new HashMap<>();
                            map.put("image", image);
                            map.put("date", dat);
                            map.put("title", title);
                            map.put("price", price);
                            map.put("status", status);
                            map.put("vendor_name", vendor_name);
                            map.put("address", address);
                            map.put("o_id", o_id);
                            map.put("staff_id", staff_id);
                            map.put("booking_status", booking_status);
                            list.add(map);
                        }
                        rv.setVisibility(View.VISIBLE);

                        rv.setAdapter(new PastBookingAdapter(context, list, datesss, new PastBookingAdapter.onClickListener() {
                            @Override
                            public void onClick(String orderId, String s, TextView tv_cancel, TextView tv_ReSchedule) {
                                RestServiceConnector.getService().cancelBooking(orderId, s, cancelBooingCallback(tv_cancel, tv_ReSchedule));
                            }

                            @Override
                            public void onRescheduleClick(String formattedDate, String time, String orderId, AlertDialog dialog2) {
                                RestServiceConnector.getService().reschedule(orderId, formattedDate, time, rescheduleCallbacks(dialog2));
                            }
                        }));
                    } else {
                        Toast.makeText(context, "size 0", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    bookingModel = null;
                    tv_no_history.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(RetrofitError arg0) {
                Toast.makeText(context, arg0.getMessage(), Toast.LENGTH_SHORT).show();
            }
        };
    }

}
