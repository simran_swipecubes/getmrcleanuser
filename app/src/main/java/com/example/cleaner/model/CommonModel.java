package com.example.cleaner.model;

import com.google.gson.annotations.SerializedName;

public class CommonModel {

    /**
     * status : 200
     * msg : Password changed successfully
     */

    @SerializedName("status")
    private int status;
    @SerializedName("msg")
    private String msg;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
