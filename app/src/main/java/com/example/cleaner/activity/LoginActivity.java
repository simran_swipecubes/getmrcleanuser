package com.example.cleaner.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.example.cleaner.R;
import com.example.cleaner.apiservice.CustomCallBacks;
import com.example.cleaner.apiservice.RestServiceConnector;
import com.example.cleaner.model.LoginModel;
import com.example.cleaner.util.NetworkUtil;
import com.example.cleaner.util.SharedPref;
import com.example.cleaner.util.Util;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    TextView txt_forgot_password, txt_sign_up, txt_login, login_forgot_password;
    EditText et_password, et_email;
    String email_login, password_login;
    Context context;
    SharedPref pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = LoginActivity.this;
        pref = new SharedPref(context);
        inItUi();
        //version();
    }

    public void inItUi() {
        txt_sign_up = findViewById(R.id.login_sign_up);
        login_forgot_password = findViewById(R.id.login_forgot_password);
        txt_login = findViewById(R.id.login_login);
        et_password = findViewById(R.id.login_password);
        et_email = findViewById(R.id.login_email);
        txt_forgot_password = findViewById(R.id.login_forgot_password);
        txt_forgot_password.setOnClickListener(this);
        txt_login.setOnClickListener(this);
        txt_sign_up.setOnClickListener(this);
    }

    public void getFirebaseToken() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this,
                new OnSuccessListener<InstanceIdResult>() {
                    @Override
                    public void onSuccess(InstanceIdResult instanceIdResult) {
                        String newToken = instanceIdResult.getToken();
                        Log.e("newToken", newToken);
                        pref.setToken(newToken);
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_login:
                if (NetworkUtil.INSTANCE.isConnected(context)) {
                    validate();
                }
                break;
            case R.id.login_sign_up:
                Intent i = new Intent(context, SignupActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.rightsideanimation, R.anim.leftoutanimation);
                break;
            case R.id.login_forgot_password:
                Intent i1 = new Intent(context, ForgetPasswordActivity.class);
                startActivity(i1);
                break;
        }
    }

    private void validate() {
        email_login = et_email.getText().toString().trim();
        password_login = et_password.getText().toString().trim();
        if (!Util.INSTANCE.isValidEmail(email_login)) {
            et_email.setError("Invalid Email");
            et_email.requestFocus();
            et_email.findFocus();

        } else if (password_login.length() < 5) {
            et_password.setError("Check Password");
            et_password.requestFocus();
            et_password.findFocus();
        } else {
            RestServiceConnector.getService().login(email_login, password_login, "android",
                    pref.getToken(), Login());
        }
    }

    // ********************* Calling Register/SignIn Api *****************************
    public CustomCallBacks<LoginModel> Login() {
        return new CustomCallBacks<LoginModel>(context, true) {
            @Override
            public void onSucess(LoginModel arg0, Response arg1) {
                if (arg0.getStatus() == 200) {
                    LoginModel.DataBean data = arg0.getData();
                    pref.setUSER_ID(data.getId());
                    pref.setUSER_EMAIL(data.getEmail());
                    pref.setUSER_NAME(data.getFirstname());
                    pref.setUSER_LAST_NAME(data.getLastName());
                    pref.setUSER_MOB(data.getPhone());
                    pref.setADDRESS_CITY(data.getCity());
                    pref.setADDRESS_STATE(data.getState());
                    pref.setADDRESS_ZIPCODE(data.getZip());
                    pref.setADDRESS_ADDRESS(data.getAddress());
                    Log.d("TAG", "" + data.getId());
                    startActivity(new Intent(context, MainActivity.class));
                    overridePendingTransition(R.anim.rightsideanimation, R.anim.leftoutanimation);
                    finish();
                } else {
                    Toast.makeText(context, arg0.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(RetrofitError arg0) {
                Toast.makeText(context, arg0.getMessage(), Toast.LENGTH_SHORT).show();
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        getFirebaseToken();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }
}
