package com.example.cleaner.activity;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.cleaner.R;
import com.example.cleaner.adapter.SubServicesAdapter;
import com.example.cleaner.apiservice.CustomCallBacks;
import com.example.cleaner.apiservice.RestServiceConnector;
import com.example.cleaner.model.GetSubServicesModel;
import org.json.JSONArray;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.HashMap;
import java.util.Map;

public class DetailsServiceActivity extends AppCompatActivity implements View.OnClickListener {

    private Context mContext = this;
    private TextView actionbar_title, tv_title, tv_description, tv_no_services, tv_startingPrice, tv_detail_continue;
    private ImageView action_bar_back;
    private RecyclerView rv_sub_services;
    Intent intent;
    String service_id, title, description, starting_price,methods_id,unit_id;
    int total = 0, sub_service_price = 0;
    JSONArray sub_servicesArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        initUi();
    }

    private void initUi() {
        intent = getIntent();

        service_id = intent.getStringExtra("service_id");
        description = intent.getStringExtra("description");
        title = intent.getStringExtra("title");
        starting_price = intent.getStringExtra("starting_Price");
        methods_id = intent.getStringExtra("methods_id");
        unit_id = intent.getStringExtra("unit_id");
        actionbar_title = findViewById(R.id.actionbar_title);
        tv_title = findViewById(R.id.tv_title);
        tv_no_services = findViewById(R.id.tv_no_services);
        tv_description = findViewById(R.id.tv_description);
        rv_sub_services = findViewById(R.id.rv_sub_services);
        tv_startingPrice = findViewById(R.id.tv_startingPrice);
        tv_detail_continue = findViewById(R.id.tv_detail_continue);
        actionbar_title.setText(getResources().getString(R.string.detail));
        action_bar_back = findViewById(R.id.action_bar_back);

        tv_title.setText(title);
        tv_startingPrice.setText("$  " + starting_price);
        tv_description.setText(Html.fromHtml(description).toString());

        Map<String, String> data = new HashMap<String, String>();
        data.put("sid", service_id);
        RestServiceConnector.getService().getSubServicesLIst(data, subServicesListCallback());

        action_bar_back.setOnClickListener(this);
        tv_detail_continue.setOnClickListener(this);
        total = Integer.valueOf(starting_price);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.action_bar_back:
                onBackPressed();
                break;
            case R.id.tv_detail_continue:
                Intent intent = new Intent(mContext, BookAppointmentActivity.class);
                if (sub_servicesArray != null) {
                    intent.putExtra("sub_servicesArray", sub_servicesArray.toString());
                }
                intent.putExtra("total", total+"");
                intent.putExtra("service_price", starting_price);
                intent.putExtra("methods_id", methods_id);
                intent.putExtra("unit_id", unit_id);
                intent.putExtra("service_id", service_id);
                startActivity(intent);
                break;
        }
    }


    // ************* get sub Services List  APi is here ***********
    private CustomCallBacks<GetSubServicesModel> subServicesListCallback() {
        return new CustomCallBacks<GetSubServicesModel>(mContext, true) {
            @Override
            public void onSucess(GetSubServicesModel arg0, Response arg1) {
                if (arg0.getStatus() == 200) {
                    tv_no_services.setVisibility(View.GONE);
                    rv_sub_services.setLayoutManager(new LinearLayoutManager(mContext));
                    rv_sub_services.setAdapter(new SubServicesAdapter(mContext, arg0.getData(), new SubServicesAdapter.onClickListener() {
                        @Override
                        public void onClick(JSONArray array, String price, String type) {
                            sub_servicesArray = new JSONArray();
                            sub_service_price = Integer.valueOf(price);
                            sub_servicesArray = array;
                            if (type.equals("add")) {
                                total = total + sub_service_price;
                            } else if (type.equals("substract")) {
                                total = total - sub_service_price;
                            }

                            Log.e("TAG", total + "");
                            Log.e("TAG", array + "");
                        }
                    }));

                } else if (arg0.getStatus() == 201) {
                    tv_no_services.setVisibility(View.VISIBLE);
                    Toast.makeText(mContext, arg0.getMsg(), Toast.LENGTH_SHORT).show();
                } else {
                    tv_no_services.setVisibility(View.VISIBLE);
                    Toast.makeText(mContext, arg0.getMsg(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(RetrofitError arg0) {

                Toast.makeText(mContext, arg0.getMessage(), Toast.LENGTH_SHORT).show();

            }
        };
    }
}
