package com.example.cleaner.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.example.cleaner.R;
import com.example.cleaner.apiservice.CustomCallBacks;
import com.example.cleaner.apiservice.RestServiceConnector;
import com.example.cleaner.model.CommonModel;
import com.example.cleaner.util.Util;
import com.google.android.material.textfield.TextInputEditText;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ForgetPasswordActivity extends AppCompatActivity implements View.OnClickListener {
    TextInputEditText forget_email;
    TextView tv_Reset;
    Context mContext = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        InitUis();
    }

    private void InitUis() {
        forget_email = findViewById(R.id.forget_email);
        tv_Reset = findViewById(R.id.tv_Reset);

        tv_Reset.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_Reset:
                validations();
                break;
        }
    }

    private void validations() {
        if (!Util.INSTANCE.isValidEmail(forget_email.getText().toString())) {
            forget_email.setError("Invalid Email");
            forget_email.requestFocus();
            forget_email.findFocus();
        } else {
            RestServiceConnector.getService().forgetPassword(forget_email.getText().toString(), forgetPasswordCallback());
        }
    }

    // ***************** forget password APi is here **************
    private CustomCallBacks<CommonModel> forgetPasswordCallback() {
        return new CustomCallBacks<CommonModel>(mContext, true) {
            @Override
            public void onSucess(CommonModel arg0, Response arg1) {
                if (arg0.getStatus() == 200) {
                    Toast.makeText(mContext, arg0.getMsg(), Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(mContext, LoginActivity.class));
                    finish();
                } else {
                    Toast.makeText(mContext, arg0.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(RetrofitError arg0) {
                Toast.makeText(mContext, arg0.getMessage(), Toast.LENGTH_SHORT).show();

            }
        };
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
