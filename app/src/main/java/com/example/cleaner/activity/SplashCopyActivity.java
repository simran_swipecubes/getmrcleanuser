package com.example.cleaner.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import com.example.cleaner.R;
import com.example.cleaner.activity.LoginActivity;
import com.example.cleaner.activity.MainActivity;
import com.example.cleaner.activity.SplashActivity;
import com.example.cleaner.common.AppController;
import com.example.cleaner.common.PrefStore;

import com.example.cleaner.util.SharedPref;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.List;
import java.util.Locale;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CALL_PHONE;


public class SplashCopyActivity extends Activity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {
    final static int REQUEST_LOCATION = 199;
    private static final int PERMISSION_REQUEST_CODE = 100;
    public static double lat = 0, lng = 0;
    public static String address = "", postalCode, strAdd = "", city,state;
    boolean statusOfGPS = false;
    PrefStore sharepreference;
    ImageView imageLogo;
    Animation animation;
    boolean isGPSEnabled = false;
    boolean isNetworkEnabled = false;
    boolean isPassive = false;
    Location location;
    LocationListener listener;
    private GoogleApiClient googleApiClient;
    Dialog dialog;
    protected LocationManager locationManager;
    Context mContext = this;
    FusedLocationProviderClient fusedLocationClient;
    LocationCallback mLocationCallback;
    SharedPref pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (AppController.isNetworkAvailable(mContext)) {
            inItUi();
        } else {
            internetCheckDialog();
        }

    }

    public void inItUi() {
        pref = new SharedPref(this);
        FirebaseApp.initializeApp(this);
        sharepreference = new PrefStore(SplashCopyActivity.this);
        //  imageLogo = findViewById(R.id.imageLogo);
        animation = AnimationUtils.loadAnimation(this, R.anim.animation_bottom_up);
        try {
            LocationManager manager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
            statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (!statusOfGPS) {
                enableLoc();
            } else {
                requestPermission();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        getFirebaseToken();
    }

    @TargetApi(Build.VERSION_CODES.M)

    private void requestPermission() {
        if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_CODE);
        } else {
            getLocation();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d("Permission", "accepted");
                    getLocation();
                    // checkLogin();
                } else {
                    Log.d("Permission", "denied");
                    finish();
                }
                break;
        }
        Log.d("Permission", "Outer");
    }

    public void checkLogin() {
        //  imageLogo.setVisibility(View.VISIBLE);
        //    imageLogo.startAnimation(animation);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (pref.getUSER_ID().isEmpty()){
                    // getLocation();
                    Intent i = new Intent(SplashCopyActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();

                } else{
                    Intent i = new Intent(SplashCopyActivity.this, MainActivity.class);
                    startActivity(i);
                    finish();
                    //  getLocation();

                }
            }
        }, 1500);
        fusedLocationClient.removeLocationUpdates(mLocationCallback);

    }

    // ****************************** Get User CurrentLocation *****************************

    public void getLocation() {

        getSplashLocation();

    }

    private void setLoc(Location loc) {
        lng = loc.getLongitude();
        lat = loc.getLatitude();
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            if (addresses != null && addresses.size() > 0) {
                Address returnedAddress = addresses.get(0);
                address = returnedAddress.getAddressLine(0);
                city = addresses.get(0).getLocality();
                state = addresses.get(0).getAdminArea();
                postalCode = addresses.get(0).getPostalCode();
                sharepreference.saveString("latitude", lat + "");
                sharepreference.saveString("longitude", lng + "");
                sharepreference.saveString("address", String.valueOf(strAdd));
                sharepreference.saveString("city", String.valueOf(city));
                sharepreference.saveString("zip", String.valueOf(postalCode));
                Log.d("TAG:", city);
                Log.d("TAG:", address);
                checkLogin();

            } else {
                getSplashLocation();

                Log.d("TAG", "No Address returned!");

            }
        } catch (Exception e) {
            e.printStackTrace();
            getSplashLocation();
            Log.d("TAG", "Cant get Address!");
        }
    }

    private void enableLoc() {
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(@Nullable Bundle bundle) {

                        }

                        @Override
                        public void onConnectionSuspended(int i) {

                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(ConnectionResult connectionResult) {
                            Log.d("Location error ", connectionResult.getErrorCode() + "");
                        }
                    }).build();
            googleApiClient.connect();
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);
            builder.setAlwaysShow(true);
            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {

                                status.startResolutionForResult(SplashCopyActivity.this, REQUEST_LOCATION);
                            } catch (IntentSender.SendIntentException e) {
                                Toast.makeText(SplashCopyActivity.this, "Errror", Toast.LENGTH_SHORT).show();
                            }
                            break;
                    }
                }
            });
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }

    public void getFirebaseToken() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(SplashCopyActivity.this,
                new OnSuccessListener<InstanceIdResult>() {
                    @Override
                    public void onSuccess(InstanceIdResult instanceIdResult) {
                        String newToken = instanceIdResult.getToken();
                        Log.d("TAG", "newTokenhere :" + newToken);
                        PrefStore prefStore = new PrefStore(SplashCopyActivity.this);
                        prefStore.saveString("refreshedToken", newToken);
                        //  SharedPreferencesHelper.getInstance(mContext).setRegistrationToken(newToken);
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_LOCATION) {
            Log.d("TAG", "mmmmmmmm");
            if (resultCode == Activity.RESULT_OK) {
                requestPermission();
            } else {
                finish();
            }
        }
    }

    //*************************location update code****************************//
    protected synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();
    }

    public void getSplashLocation() {
       // progressDialog();
        try {

            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }


            mLocationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    for (Location location : locationResult.getLocations()) {
                        Log.i("MainActivity", "Location: " + location.getLatitude() + " " + location.getLongitude());
                     //   dialog.dismiss();
                        setLoc(location);

                    }
                }

            };
            LocationRequest mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(120000); // two minute interval
            mLocationRequest.setFastestInterval(120000);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
            fusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
            //   fusedLocationClient.removeLocationUpdates(mLocationCallback);

            /*fusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if (location != null) {
                        lat = location.getLatitude();
                        lng = location.getLongitude();
                        dialog.dismiss();
                        setLoc(location);

                    }
                }
            });*/


           /* locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            isPassive = locationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled && !isPassive) {

                Log.d("TAG", "GPS  and network Disabled");

            } else if (isGPSEnabled && isNetworkEnabled) {

                LocByProvider(LocationManager.NETWORK_PROVIDER);

                Log.d("TAG", "GPS  and network enale");

            } *//*else if (isPassive) {

                LocByProvider(LocationManager.PASSIVE_PROVIDER);
                Log.d("TAG", "passive enale");
            }*//* else if (isGPSEnabled) {
                LocByProvider(LocationManager.GPS_PROVIDER);
                Log.d("TAG", "gps enale");
            }*/

        } catch (Exception e) {
            Log.d("TAG", "catch");
            dialog.dismiss();
            e.printStackTrace();
        }

    }

    private void LocByProvider(final String provider) {
        listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location loc) {
                Log.d("TAG", "onLocationChanged: " + loc.getLatitude());
                Log.d("TAG", "provider: " + provider);
                dialog.dismiss();
                setLoc(loc);
                //locationManager.removeUpdates(listener);

            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };
       /* Criteria criteria = neworder Criteria();
        Log.d("TAG", "criteria "+criteria);
        criteria.setAccuracy(Criteria.ACCURACY_COARSE);
        locationManager.requestSingleUpdate(criteria, listener, null);*/

        //   locationManager.requestLocationUpdates(provider, 1, 1, listener);
        // locationManager.requestSingleUpdate(provider,  listener);
    }

    private void progressDialog() {
        dialog = new Dialog(SplashCopyActivity.this);
        View view = View.inflate(SplashCopyActivity.this, R.layout.progress_dialog, null);
      /*  TextView txt = view.findViewById(R.id.txtMsgTV);
        txt.setText("Checking your current location");*/
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(view);

        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.BOTTOM;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("TGA", "onDestroy: ");
    }

/*
    public void alertDialog() {
        AlertDialog.Builder builder2 = new AlertDialog.Builder(SplashCopyActivity.this);
        View view2 = LayoutInflater.from(SplashCopyActivity.this).inflate(R.layout.tablebooking_dialogbox, null);
        builder2.setView(view2);
        TextView text = view2.findViewById(R.id.text);
        text.setText("Poor internet/No Internet Connection \n Check Internet connection and Try again!");
        Button ok = view2.findViewById(R.id.btn_oksearch);
        final AlertDialog dialog2 = builder2.create();
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog2.dismiss();
                finish();
            }
        });
        dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog2.setCancelable(false);
        dialog2.setCanceledOnTouchOutside(false);
        dialog2.show();
    }*/

    public void internetCheckDialog() {
        AlertDialog.Builder builder2 = new AlertDialog.Builder(SplashCopyActivity.this);
        View view2 = LayoutInflater.from(SplashCopyActivity.this).inflate(R.layout.popup_network, null);
        builder2.setView(view2);
        TextView ok = view2.findViewById(R.id.ok);
        final AlertDialog dialog3 = builder2.create();
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog3.dismiss();
                finish();
            }
        });
        dialog3.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog3.setCancelable(false);
        dialog3.setCanceledOnTouchOutside(false);
        dialog3.show();
    }
}


