package com.example.cleaner.apiservice;

import com.example.cleaner.model.*;
import retrofit.http.*;
import retrofit.mime.TypedFile;

import java.util.Map;

public interface RestApi {

    ///***************Login******************
    @FormUrlEncoded
    @POST("/userlogin.php")
    void login(
            @Field("email") String email,
            @Field("password") String pass,
            @Field("devicetype") String devicetype,
            @Field("devicetoken") String devicetoken,
            CustomCallBacks<LoginModel> callBacks);

    ///***************Get User Details******************
    //  @GET("/b2b/menu")
    //    void getB2BMenuList(@Query("RID") String RID, CustomCallBacks<RestaurantMenuListModel> callBacks);

    @GET("/userprofile.php")
    void getUserDetail(
            @Query("id") String id,
            CustomCallBacks<UserDetailModel> callBacks);

    @FormUrlEncoded
    @POST("/signup.php")
    void Signup(
            @Field("email") String email,
            @Field("password") String password,
            @Field("firstname") String firstname,
            @Field("lastname") String lastname,
            @Field("phone") String phone,
            @Field("address") String address,
            @Field("city") String city,
            @Field("state") String state,
            @Field("zip") String zip,
            @Field("lat") String lat,
            @Field("lng") String lng,
            CustomCallBacks<SignupModel> callBacks);

    @Multipart
    @POST("/updateprofile.php")
    void Update(
            @Part("firstname") String firstname,
            @Part("lastname") String lastname,
            @Part("phone") String phone,
            @Part("zip") String zip,
            @Part("address") String address,
            @Part("city") String city,
            @Part("state") String state,
            @Part("uid") String uid,
            @Part("userimg") TypedFile userimg,
            @Part("lat") String lat,
            @Part("lng") String lng,
            CustomCallBacks<UpdateProfileModel> callBacks);

    @FormUrlEncoded
    @POST("/changepass.php")
    void changePass(
            @Field("id") String email,
            @Field("oldpass") String oldpass,
            @Field("newpass") String newpass,
            CustomCallBacks<CommonModel> callBacks);

    //******************************************** Shop CategoryListApi **********************
  /*  @GET("/shop_category_list")
    void getShopList(CustomCallBacks<ShopItemList> callBacks);*/


    // ************ Add address Api is here ***********
    @FormUrlEncoded
    @POST("/addaddress.php")
    void addAddress(@Field("uid") String uid,
                    @Field("zip") String zip,
                    @Field("city") String city,
                    @Field("state") String state,
                    @Field("lat") String lat,
                    @Field("lng") String lng,
                    @Field("address") String address,
                    CustomCallBacks<CommonModel> callBacks);

    // ********* get Address List APi is here *********
    @GET("/getaddress.php")
    void getAddressList(@QueryMap Map<String, String> data, CustomCallBacks<GetAddressModel> callBacks);


    // ************* get Services Api is here ***********
    @GET("/getservices.php")
    void getServicesList(CustomCallBacks<GetServicesModel> callBacks);

    // *********** get SubService List APi is here *********
    @GET("/getsubservice.php")
    void getSubServicesLIst(@QueryMap Map<String, String> data, CustomCallBacks<GetSubServicesModel> callBacks);

    // ************* delete Address Api is here ************
    @GET("/deleteaddress.php")
    void deleteAddress(@QueryMap Map<String, String> data,
                       CustomCallBacks<CommonModel> callBacks);

    // *********** booking APi is here ********
    @FormUrlEncoded
    @POST("/book.php")
    void booking(@Field("addressid") String addressid,
                 @Field("service_id") String service_id,
                 @Field("method_id") String method_id,
                 @Field("uid") String uid,
                 @Field("method_unit_id") String method_unit_id,
                 @Field("piece") String piece,
                 @Field("total") String total,
                 @Field("bookdate") String bookdate,
                 @Field("starttime") String starttime,
                 @Field("addonservice") String addonservice,
                 @Field("mainprice") String mainprice,
                 @Field("discount") String discount,
                 @Field("tax") String tax,
                 CustomCallBacks<CommonModel> callBacks);

    // ************* delete Address Api is here ************
    @GET("/mybooking.php")
    void BookingsList(@QueryMap Map<String, String> data,
                      CustomCallBacks<BookingModel> callBacks);

    // ************* cancel Order APi *********
    @FormUrlEncoded
    @POST("/cancel.php")
    void cancelBooking(@Field("order_id") String order_id,
                       @Field("reason") String reason,
                       CustomCallBacks<CommonModel> callBacks);

    // ************ history APi is here ********
    @GET("/history.php")
    void history(@QueryMap Map<String, String> data,
                 CustomCallBacks<HistoryModel> callBacks);

    // ************ reschedule Api is here  **********
    @FormUrlEncoded
    @POST("/reschedule.php")
    void reschedule(@Field("order_id") String order_id,
                    @Field("redate") String redate,
                    @Field("retime") String retime,
                    CustomCallBacks<CommonModel> callBacks);

    //  *********** forget Password APi is here *********
    @FormUrlEncoded
    @POST("/forgotpassword.php")
    void forgetPassword(@Field("user_email") String user_email,
                        CustomCallBacks<CommonModel> callBacks);

    // ************* rate Vendor Api is here ********
    @FormUrlEncoded
    @POST("/rating.php")
    void rating(@Field("vendor_id") String vendor_id,
                @Field("order_id") String order_id,
                @Field("rating") Float rating,
                CustomCallBacks<CommonModel> callBacks);

    // *************** edit address Api is here ***********
    @GET("/editaddress.php")
    void editAddress(@QueryMap Map<String, String> data,
                     CustomCallBacks<EditAddressModel> callBacks);

    // *************** update Address APi is here ************
    @FormUrlEncoded
    @POST("/updateaddress.php")
    void updateAddress(@Field("aid") String aid,
                       @Field("zip") String zip,
                       @Field("city") String city,
                       @Field("state") String state,
                       @Field("address") String address,
                       @Field("lat") String lat,
                       @Field("lng") String lng,
                       CustomCallBacks<CommonModel> callBacks);

    // ******************* support ticket APin is here ************
    @FormUrlEncoded
    @POST("/support_ticket.php")
    void supportTicket(@Field("uid") String uid,
                       @Field("subject") String subject,
                       @Field("message") String message,
                       CustomCallBacks<CommonModel> callBacks);


    // ********************** terms and Conditions *********************
    @GET("/terms.php ")
    void getTerms(CustomCallBacks<TermsModel> callBacks);
}
