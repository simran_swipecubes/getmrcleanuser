package com.example.cleaner.receiver

import androidx.appcompat.app.AppCompatActivity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.view.View
import com.example.cleaner.util.NetworkUtil
import com.google.android.material.snackbar.Snackbar

class NetworkChangeCheckReceiver : BroadcastReceiver() {

    private var firstTime = false

    override fun onReceive(context: Context, intent: Intent) {

        if (NetworkUtil.isConnectedBroadCast(context)) {

            if (firstTime) {
                Snackbar.make(
                    (context as AppCompatActivity).findViewById<View>(android.R.id.content),
                    "Connected",
                    Snackbar.LENGTH_SHORT
                ).show()
            }
            firstTime = true

        } else {
            Snackbar.make(
                (context as AppCompatActivity).findViewById<View>(android.R.id.content),
                "No Internet Connection",
                Snackbar.LENGTH_SHORT
            ).show()
        }

    }
}