package com.example.cleaner.activity;


import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.*;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import com.example.cleaner.R;
import com.example.cleaner.apiservice.CustomCallBacks;
import com.example.cleaner.apiservice.RestServiceConnector;
import com.example.cleaner.model.SignupModel;
import com.example.cleaner.model.TermsModel;
import com.example.cleaner.util.NetworkUtil;
import com.example.cleaner.util.SharedPref;
import com.example.cleaner.util.Util;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SignupActivity extends AppCompatActivity implements View.OnClickListener {
    TextView txt_sign_up, txt_login;
    EditText et_first_name, et_last_name, et_password, et_email, et_phone, et_address, et_zip, et_city, et_state;
    String first_name, last_name, email, password, phone, zip, city, state, address, device_type, reg_id = "", lat = "", lng = "", mapActivity;
    Context context;
    CheckBox chk_terms_conditions;
    SharedPref pref;
    RecyclerView rv_services;
    LinearLayout ll_services;
    ImageView iv_arrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        context = SignupActivity.this;
        pref = new SharedPref(context);
        inItUi();
        //version();
    }

    public void inItUi() {
        et_first_name = findViewById(R.id.sign_up_first_name);
        et_last_name = findViewById(R.id.sign_up_last_name);
        txt_sign_up = findViewById(R.id.sign_up_sign_up);
        et_email = findViewById(R.id.sign_up_email);
        txt_login = findViewById(R.id.sign_up_login);
        et_password = findViewById(R.id.sign_up_password);
        et_phone = findViewById(R.id.sign_up_phone);
        et_zip = findViewById(R.id.sign_up_zip);
        et_city = findViewById(R.id.sign_up_city);
        et_state = findViewById(R.id.sign_up_state);
        et_address = findViewById(R.id.sign_up_address);
        chk_terms_conditions = findViewById(R.id.chk_terms_conditions);
        et_state.setSelected(true);
        et_zip.setEnabled(false);
        et_city.setEnabled(false);
        et_state.setEnabled(false);
        //et_address.setFocusable(true);

        txt_sign_up.setOnClickListener(this);
        txt_login.setOnClickListener(this);
        chk_terms_conditions.setOnClickListener(this);
        // et_address.setOnClickListener(this);
        et_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_address.setFocusable(true);
                Intent i = new Intent(SignupActivity.this, MapActivity.class).putExtra("type", "sign_up");
                startActivityForResult(i, 11);
            }
        });

        if (SplashActivity.address == null) {
            et_address.setText(address);
            et_zip.setText(zip);
            et_state.setText(state);
            et_city.setText(city);
        } else {
            et_address.setText(SplashActivity.address);
            et_zip.setText(SplashActivity.pinCode);
            et_state.setText(SplashActivity.state);
            et_city.setText(SplashActivity.city);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_up_sign_up:
                if (NetworkUtil.INSTANCE.isConnected(context)) {
                    validateField();
                }
                break;
            case R.id.sign_up_login:
                //Intent i = new Intent(context, LoginActivity.class);
                // startActivity(i);
                overridePendingTransition(R.anim.rightsideanimation, R.anim.leftoutanimation);
                finish();
                break;
            case R.id.chk_terms_conditions:
                RestServiceConnector.getService().getTerms(termsCallback());
                break;
          /*  case R.id.et_address:
                Toast.makeText(context, "Hello click", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(SignupActivity.this, MapActivity.class);
                startActivityForResult(i, 11);
                break;*/
        }
    }

    private void validateField() {
        first_name = et_first_name.getText().toString().trim();
        last_name = et_last_name.getText().toString().trim();
        email = et_email.getText().toString().trim();
        password = et_password.getText().toString().trim();
        phone = et_phone.getText().toString().trim();
        zip = et_zip.getText().toString().trim();
        city = et_city.getText().toString().trim();
        state = et_state.getText().toString().trim();
        address = et_address.getText().toString().trim();

        if (first_name.length() < 2) {
            et_first_name.setError("Name length can't be less than 2");
            et_first_name.requestFocus();
            et_first_name.findFocus();
        } else if (last_name.length() < 2) {
            et_last_name.setError("Name length can't be less than 2");
            et_last_name.requestFocus();
            et_last_name.findFocus();
        } else if (!Util.INSTANCE.isValidEmail(email)) {
            et_email.setError("Invalid Email");
            et_email.requestFocus();
            et_email.findFocus();
        } else if (email.length() < 9) {
            et_email.setError("Invalid Number");
            et_email.requestFocus();
            et_email.findFocus();
        } else if (password.length() < 5) {
            et_password.setError("Password length can't be less than 6");
            et_password.requestFocus();
            et_password.findFocus();
        } else if (phone.length() < 10) {
            et_phone.setError("Invalid Phone Number");
            et_phone.requestFocus();
            et_phone.findFocus();
        } else if (address.length() < 3) {
            et_address.setError("Address length can't be less than 3");
            et_address.requestFocus();
        } else if (zip.isEmpty()) {
            et_zip.setError("Please Select Address first");
            et_zip.requestFocus();
        } else if (city.length() < 3) {
            et_city.setError(" Please Select Address first");
            et_city.requestFocus();
        } else if (state.length() < 3) {
            et_state.setError("Please Select Address first");
            et_state.requestFocus();
        } else if (!chk_terms_conditions.isChecked()) {
            Toast.makeText(context, "Please select Terms and Conditions", Toast.LENGTH_SHORT).show();
        } else {
            if (lat.equals("") && lng.equals("")) {
                RestServiceConnector.getService().Signup(email, password, first_name, last_name, phone, address, city, state, zip,
                        SplashActivity.lat + "", SplashActivity.lng + "", signup());
            } else {
                RestServiceConnector.getService().Signup(email, password, first_name, last_name, phone, address, city, state, zip, lat, lng, signup());
            }
        }
    }

    /*/********************* Calling Register/SignIn Api*****************************/
    public CustomCallBacks<SignupModel> signup() {
        return new CustomCallBacks<SignupModel>(context, true) {
            @Override
            public void onSucess(SignupModel arg0, Response arg1) {
                if (arg0.getStatus() == 200) {
                    finish();
                    overridePendingTransition(R.anim.rightsideanimation, R.anim.leftoutanimation);
                    finish();
                    Toast.makeText(context, arg0.getMsg(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, arg0.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(RetrofitError arg0) {
                Toast.makeText(context, arg0.getMessage(), Toast.LENGTH_SHORT).show();
            }
        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            return;
        }
        if (requestCode == 11) {
            et_address.clearFocus();
            address = data.getStringExtra("txt_location");
            lat = data.getStringExtra("lat");
            lng = data.getStringExtra("lng");
            zip = data.getStringExtra("pincode");
            state = data.getStringExtra("state");
            city = data.getStringExtra("city");
            mapActivity = data.getStringExtra("mapActivity");
            et_address.setText(address);
            et_zip.setText(zip);
            et_state.setText(state);
            et_city.setText(city);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /*// *************** for pdf ***************
    public String getPDFPath(Uri uri) {
        final String id = DocumentsContract.getDocumentId(uri);
        final Uri contentUri = ContentUris.withAppendedId(
                Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }*/

    //  ******************* delete address dialog ********************
    private void logout_Dialog(String data) {
        final AlertDialog.Builder builder2 = new AlertDialog.Builder(context);
        View view2 = LayoutInflater.from(context).inflate(R.layout.popup_terms, null);
        TextView cancel = view2.findViewById(R.id.cancel);
        TextView ok = view2.findViewById(R.id.ok);
        TextView txt = view2.findViewById(R.id.txt);
        txt.setText(data);
        builder2.setView(view2);
        final AlertDialog dialog2 = builder2.create();
        dialog2.setCancelable(false);
        dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog2.dismiss();
                chk_terms_conditions.setChecked(true);
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog2.dismiss();
                chk_terms_conditions.setChecked(false);
            }
        });
        dialog2.show();
    }

    private CustomCallBacks<TermsModel> termsCallback() {
        return new CustomCallBacks<TermsModel>(context, true) {
            @Override
            public void onSucess(TermsModel arg0, Response arg1) {
                if (arg0.getStatus() == 200) {
                    logout_Dialog(arg0.getData());
                } else {
                    Toast.makeText(SignupActivity.this, arg0.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(RetrofitError arg0) {
                Toast.makeText(SignupActivity.this, arg0.getMessage(), Toast.LENGTH_SHORT).show();

            }
        };
    }
}
